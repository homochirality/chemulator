/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// NOLINT(build/header_guard)

#ifndef LIB_CPP_SIMULATIONRESULTS_H_
#define LIB_CPP_SIMULATIONRESULTS_H_

#include <iomanip>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

/**
 * @brief An object of this class holds the concentrations of executed simulation
 */
class SimulationResults {

  using MapStringVectorDouble = std::unordered_map<std::string, std::vector<double>>;

  /**
   * @brief Total number of concentrations per species generated on the simulation
   */
  unsigned int numTimeIntervals;
  /**
   * @brief Other columns with data that may be relevant, for example, when multiple
   *        simulations are concatenated together
   */
  std::vector<std::string> additionalColumns;
  /**
   * @brief Species for which concentrations have been saved
   */
  std::vector<std::string> species;
  /**
   * @brief The actual values for each one of the concentrations in each time step,
   *        in the simulation. Not only concentrations are saved in this variable,
   *        also the values given to each additional column in each time step and
   *        the value for the time step.
   */
  std::shared_ptr<MapStringVectorDouble> concentrationsThruTime;

  public:
    /**
     * @brief Creates a SimulationResults object from a single simulation data run.
     *
     * @param concentrations Each concentration on the a simulation, with its name.
     *                       One of the "concentrations" is "Time", and its
     *                       mandatory, the creation of the object will fail if
     *                       there is no "Time" concentration.
     */
    explicit SimulationResults(std::shared_ptr<MapStringVectorDouble> concentrations);
    /**
     * @brief Same as above but it receives and additional parameter that specifies
     *        which "concentrations" are in fact additional columns and not species
     *
     * @param additionalCols A list of names with columns in the "concentrations"
     *                       map that are not species
     */
    explicit SimulationResults(std::shared_ptr<MapStringVectorDouble> concentrations, const std::vector<std::string> &additionalCols);
    /**
     * @brief Concatenates multiple SimulationResults objects into a single object,
     *        each object can additionally have some unique values for new columns
     *
     * @param nameNewColumns          Name of each new column to be added to the
     *                                new SimulationResults object
     * @param multipleConcentrations  A list of each new SimulationResult with the
     *                                values for each one of the new columns
     */
    explicit SimulationResults(std::vector<std::string> nameNewColumns,
                               std::vector<std::pair<std::vector<double>, SimulationResults>> multipleConcentrations);

    SimulationResults(const SimulationResults &) = default;
    SimulationResults(SimulationResults &&) = default;
    SimulationResults& operator=(const SimulationResults &) = default;
    SimulationResults& operator=(SimulationResults &&) = default;
    ~SimulationResults() = default;

    std::shared_ptr<MapStringVectorDouble>
      getConcentrationsThruTime()      const { return concentrationsThruTime; }
    int getNumTimeIntervals()          const { return numTimeIntervals;       }
    int getNumSpecies()                const { return species.size();         }
    const std::vector<std::string>& getSpecies() const { return species;                }

    /**
     * @brief Produces a string which can be saved and used as input for a gnuplot script
     */
    std::string toPlainText();
    /**
     * @brief Creates a new SimulationResults object by removing all simulation steps from
     *        0 to start-1, equivalently, returns a new object with simulation value
     *        steps starting from `start`.
     */
    SimulationResults slice(unsigned int start);
    /**
     * @brief Returns a `slice` of the current SimulationResults object, i.e., returns a
     *        copy of only the elements between the time steps `start` and `end`
     */
    SimulationResults slice(unsigned int start, unsigned int end);
    /**
     * @brief Works as slice() but the time step from which to return the new
     *        SimulationResults object is not `start`, but `end` counting backwards.
     */
    SimulationResults sliceEnd(unsigned int end);
};

#endif // LIB_CPP_SIMULATIONRESULTS_H_
