/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "simulationresults.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>

using std::pair;
using std::string;
using std::vector;
using std::shared_ptr;

SimulationResults::SimulationResults(shared_ptr<MapStringVectorDouble> concentrations)
  : SimulationResults(concentrations, vector<string>()) {}

// additionalCols is a variable holding the string values for those things that
// aren't species in concentrations (besides "Time")
SimulationResults::SimulationResults(
  shared_ptr<MapStringVectorDouble> concentrations,
  const vector<string> &additionalCols
  ) :
      numTimeIntervals(concentrations->at("Time").size())
    , concentrationsThruTime(concentrations)
{
  additionalColumns.emplace_back("Time");
  for (auto&& addCol : additionalCols) {
    additionalColumns.emplace_back(addCol);
  }
  // copying keys (species names) into species vector
  for(auto pair : *concentrations) {
    if (pair.first != "Time") {
      species.emplace_back( pair.first );
    }

      //std::cout << "* " << pair.first << " ";
      //std::copy(
          //concentrationsThruTime->at(pair.first).begin(),
          //concentrationsThruTime->at(pair.first).end(),
          //std::ostream_iterator<double>(std::cout, " "));
      //std::cout << std::endl;
  }
}

SimulationResults::SimulationResults(
    vector<string> nameNewColumns,
    vector<pair<vector<double>, SimulationResults>> multipleConcentrations)
    : concentrationsThruTime(std::make_shared<MapStringVectorDouble>())
{
  auto firstSimuResults = multipleConcentrations.front().second;

  // adding new name column and copying all old additional columns
  for (auto&& nameNewCol : nameNewColumns) {
    additionalColumns.emplace_back(nameNewCol);
    concentrationsThruTime->insert( {nameNewCol, vector<double>()} );
  }
  for(string aCol : firstSimuResults.additionalColumns) {
    additionalColumns.emplace_back(aCol);
    concentrationsThruTime->insert( {aCol, vector<double>()} );
    //std::cout << "+ "<< aCol << " ";
    //std::copy(concentrationsThruTime->at(aCol).begin(), concentrationsThruTime->at(aCol).end(), std::ostream_iterator<double>(std::cout, " "));
    //std::cout << std::endl;
  }
  //std::cout << "* ";
  //std::copy(additionalColumns.begin(), additionalColumns.end(), std::ostream_iterator<string>(std::cout, " "));
  //std::cout << std::endl;

  // adding species
  species = firstSimuResults.species;
  for(string aSpecies : species) {
    concentrationsThruTime->insert( {aSpecies, vector<double>()} );
    //std::cout << "+ "<< aSpecies << " ";
    //std::copy(concentrationsThruTime->at(aSpecies).begin(), concentrationsThruTime->at(aSpecies).end(), std::ostream_iterator<double>(std::cout, " "));
    //std::cout << std::endl;
  }
  //std::cout << "* ";
  //std::copy(species.begin(), species.end(), std::ostream_iterator<string>(std::cout, " "));
  //std::cout << std::endl;

  int newTotalSizeSimuRe = 0;
  // copying from each SimulationResults the new data and column
  for(auto pairSR : multipleConcentrations) {
    SimulationResults&& colSR = std::move(pairSR.second);

    int sizeCurrentSR = colSR.concentrationsThruTime->at("Time").size();
    int oldTotalSize = newTotalSizeSimuRe;
    newTotalSizeSimuRe += sizeCurrentSR;

    for (std::size_t i=0; i<nameNewColumns.size(); i++) {
      double colValue   = pairSR.first[i];
      string nameNewCol = nameNewColumns[i];
      concentrationsThruTime->at(nameNewCol).resize(newTotalSizeSimuRe, colValue);
    }

    for(const string &aCol : firstSimuResults.additionalColumns) {
      concentrationsThruTime->at(aCol).resize(newTotalSizeSimuRe);
      std::copy(colSR.concentrationsThruTime->at(aCol).begin(),
                colSR.concentrationsThruTime->at(aCol).end(),
                concentrationsThruTime->at(aCol).begin() + oldTotalSize
      );
      //std::cout << aCol << " ";
      //std::copy(concentrationsThruTime->at(aCol).begin(), concentrationsThruTime->at(aCol).end(), std::ostream_iterator<double>(std::cout, " "));
      //std::cout << std::endl;
    }

    for(const string &aSpecies : species) {
      concentrationsThruTime->at(aSpecies).resize(newTotalSizeSimuRe);
      std::copy(colSR.concentrationsThruTime->at(aSpecies).begin(),
                colSR.concentrationsThruTime->at(aSpecies).end(),
                concentrationsThruTime->at(aSpecies).begin() + oldTotalSize
      );
    }
  }

  numTimeIntervals = newTotalSizeSimuRe;

  //for(string aCol : additionalColumns) {
  //  std::cout << "+ "<< aCol << " ";
  //  std::copy(concentrationsThruTime->at(aCol).begin(), concentrationsThruTime->at(aCol).end(), std::ostream_iterator<double>(std::cout, " "));
  //  std::cout << std::endl;
  //}
  //for(string aSpecies : species) {
  //  std::cout << "+ "<< aSpecies << " ";
  //  std::copy(concentrationsThruTime->at(aSpecies).begin(), concentrationsThruTime->at(aSpecies).end(), std::ostream_iterator<double>(std::cout, " "));
  //  std::cout << std::endl;
  //}
}

string
SimulationResults::toPlainText()
{
  std::ostringstream oss;

  int numSpecies = species.size();
  int totalAddCols = additionalColumns.size();

  //std::cout << "Inside toPlainText" << std::endl;
  //std::cout << "numSpecies " << numSpecies << std::endl;
  //std::cout << "totalAddCols " << totalAddCols << std::endl;

  vector<double> columnData[numSpecies + totalAddCols];
  // creating header
  for (int j = 0; j < numSpecies+totalAddCols; j++) {
    string columnName = j < totalAddCols ?
                         additionalColumns[j] :
                         species.at(j - totalAddCols);

    oss << R"(")" <<  columnName << R"(")"
        << (j == numSpecies+totalAddCols-1 ? "\n" : " ");

    columnData[j] = concentrationsThruTime->at(columnName);

    //std::cout << "+ " << columnName << " ";
    //std::copy(concentrationsThruTime->at(columnName).begin(), concentrationsThruTime->at(columnName).end(), std::ostream_iterator<double>(std::cout, " "));
    //std::cout << std::endl;
  }

  oss.precision(12);
  oss << std::fixed << std::scientific;
  //std::cout.precision(12);
  //std::cout << std::fixed << std::scientific;
  for (unsigned int i = 0; i < numTimeIntervals; i++) {
    //std::cout << i << " " << numTimeIntervals;
    for (int j = 0; j < numSpecies+totalAddCols; j++) {
      oss << columnData[j].at(i)
          << (j < numSpecies+totalAddCols-1 ? " " : "\n");
      //std::cout << columnData[j].at(i)
      //          << (j < numSpecies+totalAddCols-1 ? " " : "\n");
    }
  }

  return oss.str();
}

SimulationResults
SimulationResults::slice(unsigned int start) {
  //std::cout << "In slice, with start " << start << std::endl;
  return slice(start, numTimeIntervals);
}

SimulationResults
SimulationResults::slice(unsigned int start, unsigned int end) {
  start = start > numTimeIntervals ? numTimeIntervals : start;
  end   = end   > numTimeIntervals ? 0 : numTimeIntervals - end;

  auto newConsThruTime = std::make_shared<MapStringVectorDouble>();

  // copying elements for column "Time"
  for(const string &aCol : additionalColumns) {
    vector<double> oldACol = concentrationsThruTime->at(aCol);
    vector<double> newCol;
    newCol.resize(numTimeIntervals - start - end);
    std::copy(oldACol.begin() + start,
              oldACol.end() - end,
              newCol.begin());

    //std::cout << aCol << std::endl;
    //std::copy(oldACol.begin(), oldACol.end(), std::ostream_iterator<double>(std::cout, " "));
    //std::cout << std::endl;
    //std::copy(newCol.begin(), newCol.end(), std::ostream_iterator<double>(std::cout, " "));
    //std::cout << std::endl;
    newConsThruTime->insert( { "Time", std::move(newCol)} );
  }

  // copying elements for each species column
  for(string aSpecies : species) {
    vector<double> aSpeciesCol = concentrationsThruTime->at(aSpecies);
    vector<double> newCol;
    newCol.resize(numTimeIntervals - start - end);
    //std::cout << "HERE\n";

    std::copy(aSpeciesCol.begin()+start,
              aSpeciesCol.end()-end,
              newCol.begin());
    //std::cout << aSpecies << std::endl;
    //std::copy(newCol.begin(), newCol.end(), std::ostream_iterator<double>(std::cout, " "));
    //std::cout << std::endl;

    newConsThruTime->insert( {aSpecies, std::move(newCol)} );
  }

  return SimulationResults(newConsThruTime);
}

SimulationResults
SimulationResults::sliceEnd(unsigned int end) {
  //std::cout << "in SliceEnd with " << end << " " << numTimeIntervals-end << std::endl;
  if (end > numTimeIntervals) {
    return SimulationResults(*this);
  } else {
    return slice(numTimeIntervals-end);
  }
}
