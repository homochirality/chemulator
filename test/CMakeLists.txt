# === Adding unit testing ===
include_directories(${CMAKE_HOME_DIRECTORY}/lib/c++)
include_directories(${CMAKE_HOME_DIRECTORY}/src)
include_directories(lib/catch)

set(UNIT_TEST unit_test)

set(SOURCES_UNIT_TEST
    unit/main_test.cpp
    unit/simulationdetails_test.cpp
    unit/simulationresults_test.cpp
)

add_executable(${UNIT_TEST} ${SOURCES_UNIT_TEST} ${LIB_CPPs})
set_property(TARGET ${UNIT_TEST} PROPERTY CXX_STANDARD 11)

add_test(NAME ${UNIT_TEST}_target COMMAND ${UNIT_TEST})


# === Adding property-based testing ===
add_subdirectory(lib/rapidcheck)

set(PROPERTY_TEST property_based_test_lib)

set(SOURCES_PROPERTY_TEST_LIB
    property-based/main_test_library.cpp
)

add_executable(${PROPERTY_TEST} ${SOURCES_PROPERTY_TEST_LIB} ${LIB_CPPs})
target_link_libraries(${PROPERTY_TEST} rapidcheck)
set_property(TARGET ${PROPERTY_TEST} PROPERTY CXX_STANDARD 11)

add_test(NAME ${PROPERTY_TEST}_target COMMAND ${PROPERTY_TEST})
