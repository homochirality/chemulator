TARGET = chemulator

QMAKE_CXXFLAGS += -std=c++14 -pthread

CONFIG += qt static staticlib c++14
QT += widgets svg

LIBS += -lgfortran -lquadmath

win32 {
  DEFINES += GNUPLOT_EXECUTABLE="\"\\\"C:/Program Files/gnuplot/bin/gnuplot.exe\\\"\"" \
             CMAKE_INSTALL_FULL_DATADIR="\"\\\".\\\"\""
}

!win32 {
  message("qmake compilation is currently supported only for windows, sorry :(")
}

INCLUDEPATH += lib/c++ lib/c++/optional src lib/c++/mingw-std-threads

HEADERS += src/gnuplot.h \
           src/mainwindow.h \
           src/mainwindowstate.h \
           src/qt_version_compatibility.h \
           src/reactiondetails.h \
           src/saverloader.h \
           src/simulator.h \
           lib/c++/simulationdetails.h \
           lib/c++/simulationresults.h \
           src/gnuplot_formats/gnuplotformat.h \
           src/gnuplot_formats/epsformat.h \
           src/gnuplot_formats/pbmformat.h \
           src/gnuplot_formats/pngformat.h \
           src/gnuplot_formats/svgformat.h \
           src/ui/dialogmatrices.h \
           src/ui/msgwindowgnuplotformat.h \
           src/tabs/bifurcationtab.h \
           src/tabs/standardtab.h \
           src/tabs/mainwindowtab.h \
           src/ui/newreactionswizard.cpp

SOURCES += src/main.cpp \
           src/gnuplot.cpp \
           src/mainwindow.cpp \
           src/mainwindowstate.cpp \
           src/reactiondetails.cpp \
           src/saverloader.cpp \
           src/simulator.cpp \
           lib/c++/simulationresults.cpp \
           src/gnuplot_formats/gnuplotformat.cpp \
           src/gnuplot_formats/epsformat.cpp \
           src/gnuplot_formats/pbmformat.cpp \
           src/gnuplot_formats/pngformat.cpp \
           src/gnuplot_formats/svgformat.cpp \
           src/ui/dialogmatrices.cpp \
           src/ui/msgwindowgnuplotformat.cpp \
           src/tabs/bifurcationtab.cpp \
           src/tabs/standardtab.cpp \
           src/ui/newreactionswizard.cpp \
           lib/fortran/chemical_simulator.F90 \
           lib/fortran/dlsode.for

FORMS += \
        src/ui/mainwindow.ui \
        src/tabs/ui/bifurcationtab.ui
