# Chemulator - Code documentation #

Hi, welcome to the code documentation for chemulator. Here you can see nice graphics and
all relations between the methods, classes, and functions that make up chemulator.

If you want to know how to compile and run chemulator, please go see the download page:
<https://gitlab.com/homochirality/chemulator>

If you want to modify/contribute on any part to the code, then this webpage is for you.

## General documentation ##

Chemulator's code has been documented in detail. If you want to start fiddling with the
code just take a look at the most important parts from the code: Tabs, Simulator, and the
code in Fortran.

## How to add a new tab to chemulator ##

Creating a tab from zero requires three steps:

1. Create a subclass of MainWindowTab, and implement all its methods.
2. Add a connection from mainwindow.cpp to the new class.
3. Modify the configuration files to compile

### Example ###

Next, some examples of how to do each one of these steps:

1. In this example we are gonna create a simple tab that prints "Hello World" in console.
    If you want to know how the other tabs operate, we recommend you to check out the
    source code of those classes (StandardTab, and BifurcationTab)

    First, the header file (`src/tabs/exampletab.h`):

        #ifndef EXAMPLE_TAB_H
        #define EXAMPLE_TAB_H

        #include "tabs/mainwindowtab.h"

        class ExampleTab : public MainWindowTab {
          Q_OBJECT

        public:
          explicit ExampleTab(std::unique_ptr<MainWindowState> mwstate, QWidget *parent = Q_NULLPTR);

          ~ExampleTab() = default;

          void execute(const ReactionDetails &, const SimulationDetails<double> &);
          bool isStillRunning();
          void cleanTab();
          void reapplyBoxesState();
          QJsonObject toJsonObject();
        };

        #endif  // EXAMPLE_TAB_H

    And the implementation code (`src/tabs/exampletab.cpp`):

        #include <QDebug>
        #include "tabs/exampletab.h"

        ExampleTab::ExampleTab(std::unique_ptr<MainWindowState> mwstate, QWidget *parent)
          : MainWindowTab(std::move(mwstate), parent)
        {
          qDebug() << "ExampleTab created!";
        }

        void ExampleTab::execute(const ReactionDetails &, const SimulationDetails<double> &) {
          qDebug() << "Hello World from an ExampleTab";
        }

        bool ExampleTab::isStillRunning() { return false; }
        void ExampleTab::cleanTab() {}
        void ExampleTab::reapplyBoxesState() {}

        QJsonObject ExampleTab::toJsonObject() {
          return QJsonObject { {"type", "standard"} };
        }

2. Add, in the header file of MainWindow (`src/mainwindow.h`), a pointer used to declare and initialize the tab:

        std::shared_ptr<QAction> addExampleTab;

    And add, in MainWindow (`src/mainwindow.cc`), the code in charge of declaring and
    initializing the tab:

        // ... the code above is the code in charge to add a BifurcationTab, this code goes under

        addExampleTab = make_unique<QAction>("Example", ui->add_tab_menu);
        ui->add_tab_menu->addAction(addExampleTab.get());
        connect(addExampleTab.get(), &QAction::triggered,
            [=](bool checked) {
              auto exampleTab = std::make_shared<ExampleTab>(this->getMWState(), this);
              QString tabName = "Example";
              this->addTab(tabName, std::move(exampleTab));
            }
        );

3. At last, add in the sources of `src/CMakeLists.txt` (or `chemulator.pro`, depending on
   which tool are you using to compile) the location of the code for the new tab.

   Now, just recompile and the new tab should be accessible from the tab menu, running it
   should output to console and do nothing else.
