/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "simulator.h"

#include <QDebug>
#include <QListIterator>
#include <QStringList>

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

///> The purpose of this buffer is to create a slightly longer array where the fortran code
///  will write all its output. It is very difficult to calculate precisely how many executions
///  (how much data) will fortran produce, because of rounding errors.
#define BUFFER_TIME_INTERVALS 10

namespace Simulator {

using std::unordered_map;
using std::string;
using std::vector;
using MapStringVectorDouble = unordered_map<string, vector<double>>;

// anything declared inside an anonymous namespace is only visible inside this file
namespace {
// declaring functions
SimulationResults convertConcentrationsFromFortranToMap(
                    const ReactionDetails &reactionDetails,
                    double *concentrations_through_time,
                    int numSpecies,
                    int num_time_intervals,
                    int num_time_intervals_from_fortran);

/*
 *  module chemical_simulator
 *    subroutine run_simulation(n_species, n_reactions_in, rates_in, concentrations, &
 *                              stoichiometric_matrix_in, reactions_order_matrix_in, &
 *                              T_0, Tmax, Delta, ATOL, RTOL, &
 *                              num_time_intervals, concentrations_through_time, &
 *                              output_to_stdout, failure_flag)
 *
 *      integer, intent(in)                                                         :: n_species, n_reactions_in
 *      double precision, dimension(n_species), intent(inout)                       :: concentrations ! concentrations
 *      double precision, intent(in)                                                :: T_0, Tmax, Delta, RTOL
 *      double precision, dimension(n_reactions_in), intent(in), target             :: rates_in ! reactions' rates
 *      double precision, dimension(1), intent(in)                                  :: ATOL
 *
 *      integer, intent(inout)                                                      :: num_time_intervals
 *      double precision, dimension(num_time_intervals, n_species+1), intent(inout) :: concentrations_through_time
 *      integer, dimension(n_species, n_reactions_in), intent(in), target           :: stoichiometric_matrix_in
 *      integer, dimension(n_reactions_in, n_species), intent(in), target           :: reactions_order_matrix_in
 *
 *      logical, intent(in)                                                         :: output_to_stdout
 *      logical, intent(out)                                                        :: failure_flag
 */
extern "C" {
void __chemical_simulator_MOD_run_simulation(
    int *n_species, int *n_reactions, double rates_in[],
    double concentrations[], int stoichiometric_matrix_in[],
    int reactions_order_matrix_in[], double *T_0, double *Tmax, double *Delta,
    double *ATOL, double *RTOL, int *num_time_intervals,
    double concentrations_through_time[], bool *output_to_stdout,
    bool *failure_flag);
}
/***************** END OF EXTERNAL DEFINITIONS *****************/

} // anonymous namespace

SimulationResults
run(const ReactionDetails &reactionDetails,
    const SimulationDetails<double>& simuDetails,
    Debug debug) {

  double T_0   = simuDetails.getT_0();
  double Tmax  = simuDetails.getTmax();
  double Delta = simuDetails.getDelta();
  double ATOL  = simuDetails.getATOL();
  double RTOL  = simuDetails.getRTOL();

  int numSpecies   = reactionDetails.numSpecies();
  int numReactions = reactionDetails.numReactions();
  auto *rates_in               = new double[numReactions];
  auto *concentrations         = new double[numSpecies];
  auto *stoichiometric_matrix  = new int[numSpecies * numReactions];
  auto *reactions_order_matrix = new int[numSpecies * numReactions];

  const int num_time_intervals = (Tmax - T_0) / Delta + 1 + BUFFER_TIME_INTERVALS;
  int num_time_intervals_from_fortran = num_time_intervals;
  auto concentrations_through_time =
      new double[num_time_intervals * (numSpecies + 1)];
  bool output_to_stdout, failure_flag;

  // copying initial concentrations to a format readable from fortran
  const QMap<QString, double> &initialConcentrations =
      reactionDetails.getInitialConcentrations();
  QListIterator<QString> species(reactionDetails.getSpecies());
  for (int i = 0; species.hasNext(); i++) {
    concentrations[i] = initialConcentrations.value(species.next());
  }

  // copying reactions to a format readable from fortran
  const QMap<QString, double> &reactionsRates =
      reactionDetails.getReactionsRates();
  QListIterator<QString> reactions(reactionDetails.getReactions());
  for (int i = 0; reactions.hasNext(); i++) {
    rates_in[i] = reactionsRates.value(reactions.next());
  }

  // copying matrices to a format readable from fortran
  const QMap<QPair<QString, QString>, int> &stoichiometricMatrix =
      reactionDetails.getStoichiometricMatrix();
  const QMap<QPair<QString, QString>, int> &reactionsOrderMatrix =
      reactionDetails.getReactionsOrderMatrix();

  reactions.toFront();
  for (int i = 0; reactions.hasNext(); i++) {
    species.toFront();
    QString reaction = reactions.next();
    for (int j = 0; species.hasNext(); j++) {
      QString aSpecies = species.next();
      stoichiometric_matrix[i * numSpecies + j] =
          stoichiometricMatrix.value(qMakePair(reaction, aSpecies), 0);
      reactions_order_matrix[i * numSpecies + j] =
          reactionsOrderMatrix.value(qMakePair(reaction, aSpecies), 0);
    }
  }

  if (debug == Debug::On) {
    qDebug("\nRates for reactions entered:");
    reactions.toFront();
    for (int i = 0; reactions.hasNext(); i++) {
      QString reaction = reactions.next();
      // qDebug( QString("R%1: %2").arg(i+1).arg(reaction).toLatin1().data() );
      double rate = reactionDetails.getRate(reaction);
      qDebug("%s",
             QString("R%1: %2")
                  .arg(i + 1)
                  .arg(rate, 0, 'g', 3)
                  .toLatin1().data());
    }

    qDebug("\nConcentrations entered:");
    species.toFront();
    for (int i = 0; species.hasNext(); i++) {
      QString aSpecies = species.next();
      double concentration = initialConcentrations.value(aSpecies);
      qDebug("%s",
             QString("%1: %2")
                 .arg(aSpecies)
                 .arg(concentration, 0, 'g', 3)
                 .toLatin1().data());
    }

    qDebug("\nVariables for simulation entered:");
    qDebug() << "T_0  " << T_0;
    qDebug() << "Tmax " << Tmax;
    qDebug() << "Delta" << Delta;
    qDebug() << "ATOL " << ATOL;
    qDebug() << "RTOL " << RTOL;
    qDebug("\n");
  }

  if(num_time_intervals - num_time_intervals_from_fortran == BUFFER_TIME_INTERVALS) {
    std::cerr << "WARNING: " << (num_time_intervals-BUFFER_TIME_INTERVALS) << " should be " << num_time_intervals_from_fortran << " !!!" << std::endl;
  }

  output_to_stdout = debug == Debug::On;
  __chemical_simulator_MOD_run_simulation(&numSpecies, &numReactions, rates_in, concentrations,
                                          stoichiometric_matrix, reactions_order_matrix, &T_0, &Tmax,
                                          &Delta, &ATOL, &RTOL, &num_time_intervals_from_fortran,
                                          concentrations_through_time, &output_to_stdout, &failure_flag);

  return convertConcentrationsFromFortranToMap(reactionDetails, concentrations_through_time, numSpecies,
                                               num_time_intervals, num_time_intervals_from_fortran);
}

// anything declared inside an anonymous namespace is only visible inside this file
namespace {
SimulationResults
convertConcentrationsFromFortranToMap(
              const ReactionDetails &reactionDetails,
              double *concentrations_through_time,
              int numSpecies,
              int num_time_intervals,
              int num_time_intervals_from_fortran) {

  auto contsThruTime = std::make_shared<MapStringVectorDouble>();

  const QStringList &species = reactionDetails.getSpecies();

  for (int j = 0; j <= numSpecies; j++) {
    string columnName = j == 0 ? "Time" : species.at(j - 1).toStdString();
    vector<double> columnList;
    columnList.reserve(num_time_intervals_from_fortran);

    for (int i = 0; i < num_time_intervals_from_fortran; i++) {
      columnList.push_back(
          concentrations_through_time[j * num_time_intervals + i]);
    }

    //std::cout << columnName << " ";
    //std::copy(columnList.begin(), columnList.end(), std::ostream_iterator<double>(std::cout, " "));
    //std::cout << std::endl;

    contsThruTime->insert( {columnName, std::move(columnList)} );

    //std::cout << columnName << " ";
    //std::copy(contsThruTime->at(columnName).begin(), contsThruTime->at(columnName).end(), std::ostream_iterator<double>(std::cout, " "));
    //std::cout << std::endl << std::endl;

  }

  //std::cout << contsThruTime->at("Time")[0] << std::endl;
  return SimulationResults( contsThruTime );
  // qDebug() << "array from fortran to QMapList copied";
}
} // anonymous namespace

} // namespace Simulator
