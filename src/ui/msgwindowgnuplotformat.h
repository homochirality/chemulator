/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QDialog>
#include <QDialogButtonBox>
#include <QVBoxLayout>

#include <memory>

#ifndef SRC_UI_MSGWINDOWGNUPLOTFORMAT_H_
#define SRC_UI_MSGWINDOWGNUPLOTFORMAT_H_

namespace Ui {

/*!
 * @brief Dialog displayed when asking for details for a plot
 */
class MsgWindowGnuPlotFormat : public QDialog {
public:
  //! The main layout of this Dialog, it contains the form and button widgets
  std::shared_ptr<QVBoxLayout>      verticalLayout;
  //! Widget that will contain each one of the different questions to ask the user for
  //  input
  std::shared_ptr<QWidget>          enclosingFormLayout;
  //! Ok and Cancel buttons
  std::shared_ptr<QDialogButtonBox> buttonBox;

  explicit MsgWindowGnuPlotFormat(QWidget *);
  ~MsgWindowGnuPlotFormat() = default;
};

} // namespace Ui

#endif  // SRC_UI_MSGWINDOWGNUPLOTFORMAT_H_
