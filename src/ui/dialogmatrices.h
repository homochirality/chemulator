/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QDialog>
#include <QLabel>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QWidget>

#ifndef SRC_UI_DIALOGMATRICES_H_
#define SRC_UI_DIALOGMATRICES_H_

namespace Ui {
class MainWindow;

/*!
 * @brief Dialog that shows the input reactions and the Stoichiometric, and Reactions
 *        Order Matrices
 */
class QDialogMatrices : public QDialog {
  QVBoxLayout matricesLayout;
  QTabWidget  tabsWidget;
  QLabel reactionsLabel;
  QLabel stoichiometricLabel;
  QLabel reactionsOrderLabel;

public:
  explicit QDialogMatrices(QWidget *parent = 0);
  void setReactions(const QString &);
  void setStoichiometricMatrix(const QString &);
  void setReactionsOrder(const QString &);
};
} // namespace Ui

#endif // SRC_UI_DIALOGMATRICES_H_
