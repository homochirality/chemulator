/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_REACTIONDETAILS_H_
#define SRC_REACTIONDETAILS_H_

#include <QList>
#include <QMap>
#include <QObject>
#include <QPair>
#include <QString>
#include <QStringList>

/*!
 * @brief Any object of this class is supposed to represent a chemical network, a set of
 *        reactions with their constants (kinetic constants and initial concentrations)
 *
 * The idea of this class is to be "pure", and shouldn't change, it just carries
 * information about the reactions values, a new set of reactions requires creating a new
 * object of this class
 */
class ReactionDetails {
public:
  explicit ReactionDetails(QStringList enteredReactions);
  explicit ReactionDetails(const ReactionDetails&) = default;
  ~ReactionDetails() = default;
  ReactionDetails& operator=(const ReactionDetails&) = default;

  /*!
   * @brief Returns the stoichiometric matrix for the chemical network formated to
   *        look pretty when printed in console
   */
  const QString stoichiometricMatrixAsQString() const;
  // QString stoichiometricMatrixAsPythonArray();
  /*!
   * @brief Returns the reactions order matrix for the chemical network formated to
   *        look pretty when printed in console
   */
  const QString reactionsOrderMatrixAsQString() const;
  // QString reactionsOrderMatrixAsPythonArray();
  /*!
   * @brief Returns the reactions for the chemical network formated to look pretty when
   *        printed in console
   */
  const QString reactionsAsQString() const;

  // Getters
  //! Returns the number species involved in the reaction network
  int numSpecies() const   { return species.length(); }
  //! Returns the number of reactions
  int numReactions() const { return reactions.length(); }
  /*!
   * @brief returns the rate (kinetic constant) of an specific reaction
   *
   * Careful, it is undefined what happens if you use a reaction (QString) that is
   * not in the list given by getReactions()
   *
   * @param reaction Name of the reactions, it must be one of the names returned by
   *                 getReactions()
   */
  double getRate(const QString &) const;
  /*!
   * @brief returns the initial concentration of a specific species
   *
   * Careful, it is undefined what happens when you ask for a species (a QString)
   * that is not in the list of species (getSpecies())
   *
   * @param aSpecies Name of the species to get the concentration from, the species
   *                 must be one of the species from getSpecies()
   */
  double getInitialConcentration(const QString &) const;
  //! Returns a list with all the reactions
  const QStringList& getReactions() const { return reactions; }
  //! Returns a list with all the species
  const QStringList& getSpecies() const   { return species; }
  //! Returns a list/map with all initial concentrations for the species
  const QMap<QString, double>& getInitialConcentrations() const { return initialConcentrations; }
  //! Returns a list/map with all rates (kinetic constants) for each reaction
  const QMap<QString, double>& getReactionsRates() const        { return reactionsRates; }
  //! Returns the internal representation of the Stoichiometric Matrix
  const QMap<QPair<QString, QString>, int>& getStoichiometricMatrix() const { return stoichiometricMatrix; }
  //! Returns the internal representation of the Reactions Order Matrix
  const QMap<QPair<QString, QString>, int>& getReactionsOrderMatrix() const { return reactionsOrderMatrix; }

  // Setters
  /*!
   * @brief Sets the rate of a specific reaction
   *
   * If you use a reaction name that it is not in getReactions() then the value
   * you give will pass with no problem but it will never be used in any computation
   *
   * @param reaction Name of the reaction to set the rate
   * @param rate     New rate
   */
  void setRate(const QString &, double);
  /*!
   * @brief Sets the initial concentration in the simulation for a specific species
   *
   * If you use a species name that it is not in getSpecies() then the value you give
   * will pass with no problem but it will never be used in any computation
   *
   * @param aSpecies      Name of the species to modify the initial concentration
   * @param concentration New initial concentration
   */
  void setInitialConcentration(const QString &, double);

private:
  QStringList reactions;
  QStringList species;
  QMap<QString, double> initialConcentrations;
  QMap<QString, double> reactionsRates;  //! kinetic constants
  QMap<QPair<QString, QString>, int> stoichiometricMatrix;
  QMap<QPair<QString, QString>, int> reactionsOrderMatrix;

  /*!
   * @brief Converts a string with a sum of species into a QMap
   *
   * Returns a map with all the species it had found in the string with their stoichiometric values
   * e.g., `"H2O + 4 CO2 + 3N" -> QMap(("H2O", 1)("CO2", 4)("N", 3))`.
   *
   * Assumes non zero stoichiometric values for each species.
   *
   * @param speciesInAList A string of the form `nA + mB + sC ...` where `n`, `m` and `s`
   *                       are integers, and `A`, `B` and `C` are species names with no
   *                       whitespaces
   *
   * @return A QMap where each key is the name of a species and their values are the species
   *         stoichiometric coefficients
   */
  QMap<QString, int> speciesFromSum(const QString &);
  /*!
   * @brief Adds the stoichiometric coefficients present in a reaction to the
   *        stoichiometric matrix, and the corresponding values to the
   *        reactions order matrix
   *
   * @param reaction           Reaction in QString form
   * @param speciesInTheLeft   A QMap representing the species found at the left side of the reaction
   * @param speciesInTheRight  A QMap representing the species found at the right side of the reaction
   */
  void addSpeciesToMatrices(const QString &, const QMap<QString, int>&, const QMap<QString, int>&);
  /*!
   * @brief Takes to QMaps representing the species found at the left and right side of the reaction
   *        and returns a QString representation of the reaction
   *
   * @param speciesInTheLeft   A QMap representing the species found at the left side of the reaction
   * @param speciesInTheRight  A QMap representing the species found at the right side of the reaction
   */
  QString reactionToString(const QMap<QString, int> &, const QMap<QString, int> &);
};

#endif  // SRC_REACTIONDETAILS_H_
