/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_MAINWINDOW_H_
#define SRC_MAINWINDOW_H_

#include <QDoubleValidator>
#include <QLineEdit>
#include <QMainWindow>

#include <memory>
#include <unordered_map>
#include <utility>
#include <vector>

#include "reactiondetails.h"
#include "simulationdetails.h"
#include "tabs/mainwindowtab.h"
#include "ui_mainwindow.h"

/*!
 * @brief General namespace used by default by Qt to save all UI Classes. Every *.ui
 *        file is automatically converted in a class inside this namespace.
 */
namespace Ui {
class MainWindow;
} // namespace Ui

/*!
 * @brief Principal window where all the interaction with the user happens.
 *
 * The main window is divided into three regions, the menu (up), a column (left) and a
 * space for plotting (right).
 *
 * The menu contains the basic global functionality of any program: "File" (where the
 * state of the main window can be saved), "Tab" (with functionality to add more tabs
 * or specific functionality for the current tab), and details about the state of the
 * main window (like the underlying SNA calculations).
 *
 * The left column contains boxes to set the initial variables or constants of the
 * simulation like the rate (kinetic constants), or the inital concentrations of each
 * species.
 *
 * The right area is intended to show the results of the simulation, what exactly is
 * show in the area depends on the selected tab. Each tab implements its own simulation
 * scheme, e.g., StandardTab or BifurcationTab.
 */
class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private slots:
  /*!
   * @brief Copies the current state of the values setted in the left column and passes
   *        it to the currently selected tab to execute.
   *
   * The tabs are called with a copy of \ref reactionDetails and \ref simulationDetails
   */
  void on_pushButton_clicked();
  /*!
   * @brief Opens a dialog to input a new set of reactions, and reloads the interface
   *        afterwards with the new species and reactions
   *
   * @param checked this parameter is ignored, it exists because this function is connected
   *                (using Qt `connect`) to the triggered slot which returns a bool
   */
  void when_new_reactions_is_pushed(bool);
  /*!
   * @brief Opens a dialog to search for a file with extension `.simu.json` and reloads
   *        the interface with the parameters given in the file
   *
   * @param checked this parameter is ignored, it exists because this function is connected
   *                (using Qt `connect`) to the triggered slot which returns a bool
   */
  void when_load_reactions_is_pushed(bool);
  /*!
   * @brief Opens a dialog to select a name and directory to save the current set of reactions
   *        and setted variables
   *
   * @param checked this parameter is ignored, it exists because this function is connected
   *                (using Qt `connect`) to the triggered slot which returns a bool
   */
  void when_save_reactions_is_pushed(bool);
  /*!
   * @brief Opens a dialog containing the stoichiometric information for the current set of
   *        reactions, namely: the stoichiometric matrix and the order reactions matrix
   *
   * @param checked this parameter is ignored, it exists because this function is connected
   *                (using Qt `connect`) to the triggered slot which returns a bool
   */
  void when_show_SNA_matrices(bool);
  /*!
   * @brief Enables/disables species and reactions boxes in the left column according to what
   *        the current tab declares them to be, and puts the actions declared by the current
   *        tab in the menu "Tab".
   *
   * Each tab declares which tabs to disable with their method \ref reapplyBoxesState()
   *
   * \see MainWindowTab
   *
   * @param index The position of the currently selected tab
   */
  void reloadTabActionsAndRestrictions(int);

private:
  /*!
   * @brief A reference to the class created from the .ui file in which the structure of
   *        MainWindow is defined
   */
  std::unique_ptr<Ui::MainWindow> ui;
  /*!
   * @brief Validator used in the boxes to force only double parameters to be entered
   */
  std::unique_ptr<QDoubleValidator> qdoublevalidator;
  //std::shared_ptr<QAction> addStandardTab;    //!< Action that will create a new standard tab
  std::unique_ptr<QAction> addBifurcationTab; //!< Action that will create a new bifurcation tab

  /*!
   * @brief Reaction details for the last execution performed
   */
  std::shared_ptr<ReactionDetails>           reactionDetails;
  /*!
   * @brief Simulation details for the last execution performed
   */
  std::shared_ptr<SimulationDetails<double>> simulationDetails;

  /*!
   * @brief Contains the boxes where the user changes the parameters to be setted
   */
  std::shared_ptr< std::unordered_map<std::string, std::shared_ptr<QLineEdit>> >
    reactionsBoxes,
    speciesBoxes; //!< same as reactionsBoxes
  /*!
   * @brief Contains the boxes for the user to set the simulation details
   */
  std::shared_ptr< SimulationDetails<std::shared_ptr<QLineEdit>> >
    simuDetailsBoxes;

  /*!
   * @brief Contains all dialogs to show to the user, currently the only dialog
   *        in the list is the dialog shown when pressing when_show_SNA_matrices()
   */
  std::unordered_map<std::string, std::unique_ptr<QDialog>>
    dialogsToShow;

  /*!
   * @brief Contains each of the window tabs opened, by default there is a single
   *        tab shown when chemulator is started, StandardTab
   */
  std::vector<std::pair<QString, std::shared_ptr<MainWindowTab>>>
    windowTabs;


  /*!
   * @brief Removes all the reactions and species from the interface and adds new
   *        reactions and species according to what it is passed to it
   *
   * @param rd          Reactions and species parameters
   * @param simuDetails Simulation parameters
   */
  void reloadBasicInterfaceUsing(std::shared_ptr<ReactionDetails>,
                                 std::shared_ptr<SimulationDetails<double>>,
                                 std::shared_ptr<std::vector<QJsonObject>> tabs = nullptr);
  /*!
   * @brief Runs reloadBasicInterfaceUsing() using the parameters given in file
   *
   * @param fileName Path to the file to open
   */
  void loadInterfaceFromFile(QString);

  /*!
   * @brief Adds a new tab to the interface and to the list windowTabs.
   *
   * @param tabName Tab's name as it is displayed on the main window
   * @param tab     A tab to add, it may be any subclass of MainWindowTab
   */
  void addTab(const QString &, std::shared_ptr<MainWindowTab>);
  /*!
   * @brief Creates and returns an object with the current state of the main window
   */
  std::unique_ptr<MainWindowState> getMWState();
  /*!
   * @brief Creates dialog to be displayed when calling when_show_SNA_matrices() is
   *        pushed
   *
   * @param rd The current reaction details
   */
  std::unique_ptr<QDialog> createMatricesDialog(const ReactionDetails &);
  /*!
   * @brief Takes the info from the left column and saves it into the reactions and siulation
   *        details objects
   */
  void copyInfoToReactionAndSimulationDetails();
};

#endif  // SRC_MAINWINDOW_H_
