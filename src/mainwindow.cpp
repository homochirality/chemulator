/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mainwindow.h"

#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QDirIterator>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QJsonObject>
#include <QJsonValue>
#include <QPair>
#include <QStandardItemModel>
#include <QTableView>
//#include <QThread> // to use with QThread::sleep(secs)
#include <QWizard>

#include <iostream>
#include <limits>
#include <tuple>

#include "optional.hpp"

#include "mainwindowstate.h"
#include "qt_version_compatibility.h"
#include "saverloader.h"
#include "simulator.h"
#include "tabs/bifurcationtab.h"
#include "tabs/standardtab.h"
#include "ui/dialogmatrices.h"
#include "ui/newreactionswizard.h"

#define data_dir CMAKE_INSTALL_FULL_DATADIR

/*
 *class MyItemDelegate : public QItemDelegate {
 *    public:
 *        QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
 *};
 */

using std::experimental::make_optional;
using std::experimental::optional;
using std::make_pair;
using std::make_unique;
using std::shared_ptr;
using std::string;
using std::tuple;
using std::unique_ptr;
using std::unordered_map;
using std::vector;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui               (make_unique<Ui::MainWindow>()),
      qdoublevalidator (make_unique<QDoubleValidator>(this)),
      addBifurcationTab(make_unique<QAction>("Bifurcation", ui->add_tab_menu))
      //ui         (shared_ptr<Ui::MainWindow>(new Ui::MainWindow(), [](Ui::MainWindow* ptr){ std::cout << "Cleaning Ui::MainWindow" << std::endl; delete ptr; })),
      //qdoublevalidator(shared_ptr<QDoubleValidator>(new QDoubleValidator(this), [](QDoubleValidator* ptr){ std::cout << "Cleaning QDoubleValidator" << std::endl; delete ptr; }))
{
  qDebug() << QCoreApplication::arguments();

  ui->setupUi(this);
  this->resize(950,560);

  qdoublevalidator->setRange(0e0, std::numeric_limits<double>::max(), 16);

  shared_ptr<QLineEdit>
    T_0_box_smart   (shared_ptr<QLineEdit>(), ui->T_0_box  ),
    Tmax_box_smart  (shared_ptr<QLineEdit>(), ui->Tmax_box ),
    Delta_box_smart (shared_ptr<QLineEdit>(), ui->Delta_box),
    ATOL_box_smart  (shared_ptr<QLineEdit>(), ui->ATOL_box ),
    RTOL_box_smart  (shared_ptr<QLineEdit>(), ui->RTOL_box );

  simuDetailsBoxes = std::make_shared< SimulationDetails<shared_ptr<QLineEdit>> >(
     T_0_box_smart,
     Tmax_box_smart,
     Delta_box_smart,
     ATOL_box_smart,
     RTOL_box_smart
  );

  tuple<shared_ptr<ReactionDetails>, shared_ptr<SimulationDetails<double>>, shared_ptr<vector<QJsonObject>>>
      rds = SaverLoader::defaultReactionsAndSimulationDetls();

  //simulationDetails = std::make_shared<SimulationDetails<double>>(0, 240000, 100, 1e-15, 1e-15);
  //qDebug() << simulationDetails->getT_0();

  reloadBasicInterfaceUsing(std::get<0>(rds), std::get<1>(rds), std::get<2>(rds));

  // Adding additional tabs from which the user can select from
  //addStandardTab = make_unique<QAction>("Standard", ui->add_tab_menu);
  //connect(addStandardTab.get(), &QAction::triggered,
  //    [=](bool checked) {
  //      auto standardTab = std::make_shared<StandardTab>(this->getMWState(), this);
  //      QString tabName = "Standard";
  //      this->addTab(tabName, std::move(standardTab));
  //    }
  //);
  ui->add_tab_menu->addAction(addBifurcationTab.get());
  connect(addBifurcationTab.get(), &QAction::triggered,
      [=](bool checked) {
        auto bifurcationTab = std::make_shared<BifurcationTab>(this->getMWState(), this);
        QString tabName = "Bifurcation";
        this->addTab(tabName, std::move(bifurcationTab));
      }
  );

  // Adding examples to list of examples
  QDirIterator examples(data_dir "/chemulator/examples", QDir::Files);
  //std::cout << data_dir "/chemulator/examples" << std::endl;

  if(! examples.hasNext()) {
    ui->menu_example_reactions->setEnabled(false);
  }
  while (examples.hasNext()) {
    QFileInfo ex(examples.next());
    //qDebug() << ex.baseName();
    //qDebug() << ex.filePath();
    // no naked pointer should be used, but I'm not quite sure how Qt manages pointers, I'm hoping Qt
    // deletes it once the parent is also deleted (TODO: search how Qt manages pointers!!)
    auto openFileAction = new QAction(this);
    openFileAction->setText( ex.baseName() );
    ui->menu_example_reactions->addAction( openFileAction );
    connect(openFileAction, &QAction::triggered,
        [=](bool checked) {
          this->loadInterfaceFromFile( ex.filePath() );
        }
    );
  }

  // Connecting actions to respective slots
  connect(ui->actionnew_reactions, SIGNAL(triggered(bool)), this, SLOT(when_new_reactions_is_pushed(bool)) );
  connect(ui->actionload_reactions, SIGNAL(triggered(bool)), this, SLOT(when_load_reactions_is_pushed(bool)) );
  connect(ui->actionsave_reactions, SIGNAL(triggered(bool)), this, SLOT(when_save_reactions_is_pushed(bool)) );
  connect(ui->actionshow_SNA_matrices, SIGNAL(triggered(bool)), this, SLOT(when_show_SNA_matrices(bool)) );

  // When the current displayed tab is changed all the behaivor associated to it should also change
  connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(reloadTabActionsAndRestrictions(int)) );
}

MainWindow::~MainWindow() {
  //std::cout << "Cleaning MainWindow" << std::endl;
  disconnect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(reloadTabActionsAndRestrictions(int)) );
  //std::cout << "Window size " << this->size().width() << " " << this->size().height() << std::endl;
}

/*
 * Steps followed in Reloading the User interface:
 * given ReactionDetails and SimulationDetails
 * (TODO: more info in the future, e.g., dialogs)
 *
 * Deleting
 * 1) delete Tabs
 * 2) delete dialogs
 * 3) delete boxes
 * 4) delete old ReactionDetails and SimulationDetails
 * Adding
 * 5) create new boxes
 * 6) setting values given in rd and simuDetails
 * 7) create new dialogs
 * 8) create tabs
 */
void
MainWindow::reloadBasicInterfaceUsing(
							shared_ptr<ReactionDetails> rd,
				      shared_ptr<SimulationDetails<double>> simuDetails,
              shared_ptr<vector<QJsonObject>> tabs) {

  if( ! rd ) {
    qDebug() << "Error: ReactionDetails is empty!!!";
    return;
  }
  if( ! simuDetails ) {
    qDebug() << "Error: SimulationDetails is empty!!!";
    return;
  }

  // 1) Cleaning tab interface
  //qDebug() << "Cleaning Tab Interface";
  ui->tabWidget->clear();
  windowTabs.clear();

  // 2) Delete dialogs
  dialogsToShow.clear();

  // 3) deleting all boxes
  // Deleting all Boxes
  if (ui->concentrationsLayout->count() > 0) {
    // Workaround to "delete" rows
    while (QLayoutItem *item = ui->concentrationsLayout->takeAt(0)) {
      item->widget()->deleteLater();
      delete item;
    }
  }

  if (ui->ratesLayout->count() > 0) {
    while (QLayoutItem *item = ui->ratesLayout->takeAt(0)) {
      item->widget()->deleteLater();
      delete item;
    }
  }
  reactionsBoxes.reset();
  speciesBoxes.reset();

  // 4) deleting old ReactionDetails and SimulationDetails
  reactionDetails.reset();
  simulationDetails.reset();

  // *) Interlude
  qDebug() << "Entered reactions";
  qDebug("%s", rd->reactionsAsQString().toUtf8().data());
  qDebug() << "Stoichiometric Matrix";
  qDebug("%s", rd->stoichiometricMatrixAsQString().toUtf8().data());
  qDebug() << "Reactions Order Matrix";
  qDebug("%s", rd->reactionsOrderMatrixAsQString().toUtf8().data());

  // 5) creating new boxes
  reactionsBoxes = std::make_shared< unordered_map<string, shared_ptr<QLineEdit>> >();
  speciesBoxes   = std::make_shared< unordered_map<string, shared_ptr<QLineEdit>> >();

  // Creating Boxes to hold values for the species and setting them
  for ( const QString &aSpeciesQt : rd->getSpecies() ) {
    auto newBox = std::make_shared<QLineEdit>(this);
    QString initialConcentration = QString::number(rd->getInitialConcentration(aSpeciesQt), 'g', 16);

    newBox->setValidator(qdoublevalidator.get());
    newBox->setPlaceholderText(initialConcentration);
    newBox->setText(initialConcentration);
    newBox->setMaximumWidth(80);

    ui->concentrationsLayout->addRow(aSpeciesQt, newBox.get());
    speciesBoxes->emplace(aSpeciesQt.toStdString(), newBox);
  }

  // Creating Boxes to hold values for the reactions rates and setting them
  for ( const QString &reactionQt: rd->getReactions() ) {
    auto newBox = std::make_shared<QLineEdit>(this);
    QString rate = QString::number(rd->getRate(reactionQt), 'g', 16);

    newBox->setValidator(qdoublevalidator.get());
    newBox->setPlaceholderText(rate);
    newBox->setText(rate);
    newBox->setMinimumWidth(60);
    newBox->setMaximumWidth(80);

    ui->ratesLayout->addRow(reactionQt, newBox.get());
    reactionsBoxes->emplace(reactionQt.toStdString(), newBox);
  }

  // 6) Writing values of the simulation in boxes
  QPair<QLineEdit*, QString> qLineEdits[] = {
      qMakePair( ui->T_0_box,   QString::number(simuDetails->getT_0(),   'g', 16) ),
      qMakePair( ui->Tmax_box,  QString::number(simuDetails->getTmax(),  'g', 16) ),
      qMakePair( ui->Delta_box, QString::number(simuDetails->getDelta(), 'g', 16) ),
      qMakePair( ui->ATOL_box,  QString::number(simuDetails->getATOL(),  'g', 16) ),
      qMakePair( ui->RTOL_box,  QString::number(simuDetails->getRTOL(),  'g', 16) )
  };

  for (QPair<QLineEdit*, QString> qline : qLineEdits) {
    qline.first->setValidator(qdoublevalidator.get());
    qline.first->setPlaceholderText(qline.second);
    qline.first->setText(qline.second);
  }

  // 7)
  //dialogsToShow = std::make_unique< unordered_map<string, unique_ptr<QDialog>> >();
  //dialogsToShow =  shared_ptr< unordered_map<string, shared_ptr<QDialog>> >(
  //      new unordered_map<string, shared_ptr<QDialog>>(),
  //      [](unordered_map<string, shared_ptr<QDialog>>* ptr){
  //        std::cout << "Cleaning dialogsToShow" << std::endl;
  //        delete ptr;
  //      }
  //  );
  dialogsToShow.emplace("matricesDialog", createMatricesDialog(*rd));

  this->reactionDetails   = rd;
  this->simulationDetails = simuDetails;

  // 8) creating new tabs and reloading tab actions

  //qDebug() << "Creating Tabs";
  if(!tabs) {
    addTab(QString("Standard"), std::make_shared<StandardTab>(this->getMWState(), this));
  } else {
    for (const QJsonObject& tabObject : *tabs) {
      QString tabType = tabObject.value("type").toString();
      QString tabName = tabObject.value("name").toString();

      //qDebug() << tabName;

      if (tabType == "standard") {
        addTab(tabName, std::make_shared<StandardTab>(this->getMWState(), this));

      } else if (tabType == "bifurcation") {
        addTab(tabName, std::make_shared<BifurcationTab>( this->getMWState(), this, make_optional(tabObject) ));

      } else {
        qDebug() << QString("Error: tab type `%1' unknown").arg(tabType);

      }
    }
  }
  reloadTabActionsAndRestrictions(0);
}

void
MainWindow::loadInterfaceFromFile(QString fileName) {
  if( !QFile::exists(fileName) ) {
    // TODO: display dialog telling the user the file wasn't found
    qInfo() << "File" << fileName << "doesn't exists";
    return;
  }

  QFile file( fileName );
  optional<std::tuple<
    std::shared_ptr<ReactionDetails>,
    std::shared_ptr<SimulationDetails<double>>,
    std::shared_ptr<vector<QJsonObject>>
  >> rds;
  // QIODevice::WriteOnly truncates an already written file
  if ( file.open(QIODevice::ReadOnly) ) {
    rds = SaverLoader::fromJson(QTextStream(&file).readAll());
    file.close();
  }

  // if rds contains something
  if(auto rds_ = rds) {
    reloadBasicInterfaceUsing(std::get<0>(*rds_), std::get<1>(*rds_), std::get<2>(*rds_));
  }
}

void
MainWindow::addTab(const QString &tabName, shared_ptr<MainWindowTab> tab) {
  ui->tabWidget->addTab(tab.get(), tabName);
  windowTabs.emplace_back(tabName, tab);
}

unique_ptr<MainWindowState>
MainWindow::getMWState() {
  return std::make_unique<MainWindowState>(
    reactionDetails->getSpecies(),
    reactionDetails->getReactions(),
    speciesBoxes,
    reactionsBoxes,
    simuDetailsBoxes
  );
}

void
MainWindow::on_pushButton_clicked() {
  auto tab = windowTabs.at( ui->tabWidget->currentIndex() ).second;

  if( tab->isStillRunning() ) {
      qDebug() << "Button pressed, tab (gnuplot probably) is still running"; // TODO: it shouldn't suppose that is gnuplot which is running, it could be fortran
      return;
  }

  this->copyInfoToReactionAndSimulationDetails();

  tab->cleanTab();

  tab->execute(*reactionDetails, *simulationDetails);
}

void
MainWindow::copyInfoToReactionAndSimulationDetails() {
  // Setting initial reactions to reactionDetails
  for ( const QString &aSpeciesQt: reactionDetails->getSpecies() ) {
    string aSpecies = aSpeciesQt.toStdString();
    double newInitialConcentration = speciesBoxes->at(aSpecies)->text().toDouble();
    reactionDetails->setInitialConcentration( aSpeciesQt, newInitialConcentration );
  }

  // Setting reactions rates to reactionDetails
  for ( const QString &reactionQt: reactionDetails->getReactions() ) {
    string reaction = reactionQt.toStdString();
    double newRate = reactionsBoxes->at(reaction)->text().toDouble();
    reactionDetails->setRate( reactionQt, newRate );
  }

  // Setting simulation values
  simulationDetails->setT_0   ( ui->T_0_box  ->text().toDouble() );
  simulationDetails->setTmax  ( ui->Tmax_box ->text().toDouble() );
  simulationDetails->setDelta ( ui->Delta_box->text().toDouble() );
  simulationDetails->setATOL  ( ui->ATOL_box ->text().toDouble() );
  simulationDetails->setRTOL  ( ui->RTOL_box ->text().toDouble() );
}

void
MainWindow::when_new_reactions_is_pushed(bool checked) { // NOLINT

  qInfo() << "Action triggered: Insert new set of reaction";

  Ui::NewReactionsWizard wizardWindow( this );

  // showing wizardWindow
  wizardWindow.exec();

  // if cancel button was pressed, don't do anything
  //if (wizardWindow.cancelButtonPressed()) { return; }
  optional<QStringList> listOfReactions = wizardWindow.getReactions();

  if( auto reacts = listOfReactions ) {
    //std::cout << "Hey!" << std::endl;
    qDebug() << "Reactions entered:" << *reacts;

    auto rd          = std::make_shared<ReactionDetails>(*reacts);
    auto simuDetails = std::make_shared<SimulationDetails<double>>(0, 240000, 100, 1e-15, 1e-15);

    reloadBasicInterfaceUsing(rd, simuDetails);
  }

  //std::cout << "Hey2!" << std::endl;
}

void
MainWindow::when_load_reactions_is_pushed(bool checked) { // NOLINT

  qInfo() << "Action triggered: Load Reactions";

  QFileDialog dialog(this);
  dialog.setAcceptMode(QFileDialog::AcceptOpen);
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setDirectory(QDir::homePath());
  dialog.setNameFilter(tr("JSON (*.simu.json)"));
  dialog.exec();

  // don't do anything else, the user hasn't chosen any file
  if( dialog.selectedFiles().isEmpty() ) { return; }

  loadInterfaceFromFile( dialog.selectedFiles().first() );
}

void
MainWindow::when_save_reactions_is_pushed(bool checked) { // NOLINT

  qInfo() << "Action triggered: Reactions Data Save";

  this->copyInfoToReactionAndSimulationDetails();

  QFileDialog dialog(this);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setDirectory(QDir::homePath());
  dialog.setNameFilter(tr("JSON (*.simu.json)"));
  dialog.setDefaultSuffix("simu.json");
  dialog.exec();

  if( dialog.selectedFiles().isEmpty() ) { return; }

  QString fileName = dialog.selectedFiles().first();

  if (QFile::exists(fileName)) { QFile::remove(fileName); }
  QFile file(fileName);
  // QIODevice::WriteOnly truncates an already written file
  if (file.open(QIODevice::WriteOnly)) {
    vector<QJsonObject> tabs;
    for (auto&& tabPair : windowTabs) {
      QJsonObject tabObject = tabPair.second->toJsonObject();
      tabObject.insert( "name", tabPair.first );
      tabs.emplace_back( tabObject );
    }

    QTextStream out(&file);
    out << SaverLoader::toJson(*reactionDetails, *simulationDetails, tabs);
    file.close();
  }
}

void
MainWindow::when_show_SNA_matrices(bool checked) { // NOLINT
  if (dialogsToShow.count("matricesDialog") > 0) {
    dialogsToShow.at("matricesDialog")->show();
  } else {
    qDebug() << "No matricesDialog found!! :S";
  }
}

/*
 *QWidget *MyItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const {
 *  qDebug() << index.data().toString();
 *  //if( index.data() != QString("") && index.data() != QString("a")) {
 *      //return NULL; // Prevent editing
 *  //}
 *  return QItemDelegate::createEditor(parent, option, index);
 *}
 */

unique_ptr<QDialog>
MainWindow::createMatricesDialog(const ReactionDetails &rd) {

  auto matricesDialog = std::make_unique<Ui::QDialogMatrices>(this);

  // Creating labels
  matricesDialog->setReactions(rd.reactionsAsQString());
  matricesDialog->setStoichiometricMatrix(rd.stoichiometricMatrixAsQString());
  matricesDialog->setReactionsOrder(rd.reactionsOrderMatrixAsQString());

  // std::move isn't necessary, it's just there for a legacy compiler (g++-4.9)
  return std::move(matricesDialog);
}

void
MainWindow::reloadTabActionsAndRestrictions(int index) {

  //qDebug("INSIDE RELOAD TAB ACTIONS");
  //qDebug() << "index" << index;
  //if(windowTabs) qDebug() << "windowTabs == true";
  //qDebug() << "windowTabs->size()" << windowTabs->size();
  //if(reactionsBoxes)   qDebug() << "reactionsBoxes == true";
  //if(speciesBoxes)     qDebug() << "speciesBoxes == true";
  //if(simuDetailsBoxes) qDebug() << "simuDetailsBoxes == true";
  if(index!=-1 // there is at least a tab in tabWidget
      && ! windowTabs.empty()
      && index < static_cast<int>(windowTabs.size()) // index isn't bigger than the saved tabs
      && reactionsBoxes // checking if it isn't nullptr. TODO: this should be changed to asserts
      && speciesBoxes
      && simuDetailsBoxes
  ) {
    //qDebug("Cleaning Tab Menu");
    ui->tab_menu->clear();
    //qDebug("Adding `Add Tab` menu");
    ui->tab_menu->addMenu( ui->add_tab_menu );
    //qDebug("Adding actions from tab");

    auto actionsForTab = windowTabs.at(index).second->getActions();
    for (shared_ptr<QAction> action: actionsForTab) {
      ui->tab_menu->addAction( action.get() );
    }

    getMWState()->enableAllBoxes();
    // asking tab to hide whatever it wants to hide
    windowTabs.at(index).second->reapplyBoxesState();
  }
  //qDebug("OUT OF RELOAD TAB ACTIONS");
}
