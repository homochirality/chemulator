/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "reactiondetails.h"

#include <QDebug>

ReactionDetails::ReactionDetails(QStringList enteredReactions)
{
  //species << "INH" << "HCN" << "CN-L" << "CN-D" << "H2O" << "AA-L" << "NH3" << "AA-D";

  for ( const QString &reaction: enteredReactions ) {
    QStringList leftAndRight = reaction.split(QRegExp(R"(\s*<?->\s*)"));
    QMap<QString, int> speciesInTheLeft  = speciesFromSum( leftAndRight.at(0) ); // calling this function will add the new species into the species QStringList
    QMap<QString, int> speciesInTheRight = speciesFromSum( leftAndRight.at(1) );

    if(reaction.contains("<->")) {
      QString reaction1 = reactionToString(speciesInTheLeft, speciesInTheRight);
      QString reaction2 = reactionToString(speciesInTheRight, speciesInTheLeft);
      addSpeciesToMatrices(reaction1, speciesInTheLeft, speciesInTheRight);
      addSpeciesToMatrices(reaction2, speciesInTheRight, speciesInTheLeft);
      reactions << reaction1 << reaction2;
    } else { // it contains just a "->"
      QString reaction_ = reactionToString(speciesInTheLeft, speciesInTheRight);
      addSpeciesToMatrices(reaction_, speciesInTheLeft, speciesInTheRight);
      reactions << reaction_;
    }
  }

  /*
   *qDebug() << species;
   *qDebug() << "Reactions";
   *qDebug() << reactions;
   *qDebug() << "Reactions Order Matrix";
   *qDebug() << reactionsOrderMatrix;
   *qDebug() << "Stoichiometric Matrix";
   *qDebug() << stoichiometricMatrix;
   */
}

void
ReactionDetails::addSpeciesToMatrices(const QString &reaction,
                                      const QMap<QString, int> &speciesInTheLeft,
                                      const QMap<QString, int> &speciesInTheRight) {

  // adding data to Reactions Order Matrix
  for ( const QString &aSpecies: speciesInTheLeft.keys() ) {
    reactionsOrderMatrix.insert(qMakePair(reaction, aSpecies), speciesInTheLeft.value(aSpecies));
  }

  // adding data to Stoichiometric Matrix
  for ( const QString &aSpecies: (speciesInTheLeft.keys() + speciesInTheRight.keys()).toSet().toList() ) {
    int leftValue  = speciesInTheLeft.value(aSpecies, 0);
    int rightValue = speciesInTheRight.value(aSpecies, 0);
    stoichiometricMatrix.insert(qMakePair(reaction, aSpecies), rightValue - leftValue);
  }
}

QString
ReactionDetails::reactionToString(const QMap<QString, int> &speciesInTheLeft, const QMap<QString, int> &speciesInTheRight) {

  QStringList toRet;

  QListIterator<QString> keys(speciesInTheLeft.keys());
  while ( keys.hasNext() ) {
    QString aSpecies = keys.next();
    int coefficient = speciesInTheLeft.value(aSpecies);
    if ( coefficient != 1 ) { toRet << QString::number(coefficient); }
    toRet << aSpecies;
    if (keys.hasNext()) { toRet << "+"; }
  }
  toRet << "->";
  keys = QListIterator<QString>( speciesInTheRight.keys() );
  while ( keys.hasNext() ) {
    QString aSpecies = keys.next();
    int coefficient = speciesInTheRight.value(aSpecies);

    if ( coefficient != 1 ) { toRet << QString::number(coefficient); }
    toRet << aSpecies;
    if (keys.hasNext()) { toRet << "+"; }
  }

  return toRet.join(" ");
}

QMap<QString, int>
ReactionDetails::speciesFromSum(const QString &speciesInAList) {

  QMap<QString, int> speciesInAReaction;

  for ( const QString &separated: speciesInAList.split(QRegExp(R"(\s*\+\s*)")) ) {

    QRegExp quantAndSpecies(R"((\d*)\s*([a-zA-Z][a-zA-Z0-9-_]*))");
    quantAndSpecies.indexIn( separated );

    QString aSpecies = quantAndSpecies.cap(2);
    if(aSpecies.isEmpty()) { continue; }

    int itsQuantity  = quantAndSpecies.cap(1).toInt();
    if( speciesInAReaction.contains(aSpecies) ) {
      speciesInAReaction.insert( aSpecies, speciesInAReaction.value(aSpecies) + (itsQuantity==0 ? 1 : itsQuantity) );
    } else {
      speciesInAReaction.insert( aSpecies, itsQuantity==0 ? 1 : itsQuantity );
    }

    if( ! species.contains(aSpecies) ) {
      species << aSpecies;
    }
  }

  return speciesInAReaction;
}

double
ReactionDetails::getRate(const QString &aSpecies) const {
  // TODO: Check for the QString to be in the list of reactions
  return reactionsRates.value(aSpecies);
}

double
ReactionDetails::getInitialConcentration(const QString &aSpecies) const {
  // TODO: Check for the QString to be in the list of species
  return initialConcentrations.value(aSpecies);
}

void
ReactionDetails::setRate(const QString &aSpecies, double rate) {
  // TODO: Check for the QString to be in the list of reactions
  reactionsRates.insert(aSpecies, rate);
  // qDebug() << aSpecies << concentration;
}

void
ReactionDetails::setInitialConcentration(const QString &aSpecies, double concentration) {
  // TODO: Check for the QString to be in the list of species
  initialConcentrations.insert(aSpecies, concentration);
}

const QString
ReactionDetails::reactionsAsQString() const {
  QStringList matrix;

  QListIterator<QString> reactions_(this->reactions);

  for (int i = 1; reactions_.hasNext(); i++) {
    QString reaction = reactions_.next();
    matrix << QString("R%1: %2\n").arg(i).arg(reaction);
  }

  return matrix.join("");
}

const QString
ReactionDetails::stoichiometricMatrixAsQString() const {
  QStringList matrix;

  QListIterator<QString> species_(this->species);
  QListIterator<QString> reactions_(this->reactions);

  reactions_.toFront();
  for (int i = 1; reactions_.hasNext(); i++) {
    QString reaction = reactions_.next();
    matrix << QString("R%1 ").arg(i);
  }

  matrix << "\n";
  for (int j = 0; species_.hasNext(); j++) {
    reactions_.toFront();
    QString aSpecies = species_.next();
    for (int i = 1; reactions_.hasNext(); i++) {
      QString reaction = reactions_.next();
      matrix << QString("%1 ").arg(stoichiometricMatrix.value(qMakePair(reaction, aSpecies), 0), 2);
    }
    matrix << QString(" %1\n").arg(aSpecies);
  }
  return matrix.join("");
}

/*
 *QString ReactionDetails::stoichiometricMatrixAsPythonArray() {
 *  QStringList matrix;
 *
 *  QListIterator<QString> species_(this->species);
 *  QListIterator<QString> reactions_(this->reactions);
 *
 *  matrix << "[";
 *  for (int j = 0; species_.hasNext(); j++) {
 *    matrix << "[";
 *    reactions_.toFront();
 *    QString aSpecies = species_.next();
 *    for (int i = 1; reactions_.hasNext(); i++) {
 *      QString reaction = reactions_.next();
 *      matrix << QString("%1").arg( stoichiometricMatrix.value(qMakePair(reaction,aSpecies), 0), 2 );
 *      if(reactions_.hasNext()) matrix << ", ";
 *    }
 *    matrix << "]";
 *    if(species_.hasNext()) matrix << ", ";
 *  }
 *  matrix << "]";
 *  return matrix.join("");
 *
 *}
 */

const QString
ReactionDetails::reactionsOrderMatrixAsQString() const {
  QStringList matrix;

  QListIterator<QString> species_(this->species);
  QListIterator<QString> reactions_(this->reactions);

  reactions_.toFront();
  for (int i = 1; reactions_.hasNext(); i++) {
    QString reaction = reactions_.next();
    matrix << QString("R%1 ").arg(i);
  }

  matrix << "\n";
  for (int j = 0; species_.hasNext(); j++) {
    reactions_.toFront();
    QString aSpecies = species_.next();
    for (int i = 1; reactions_.hasNext(); i++) {
      QString reaction = reactions_.next();
      matrix << QString("%1 ").arg(reactionsOrderMatrix.value(qMakePair(reaction, aSpecies), 0), 2);
    }
    matrix << QString(" %1\n").arg(aSpecies);
  }
  return matrix.join("");
}

/*
 *QString ReactionDetails::reactionsOrderMatrixAsPythonArray() {
 *  QStringList matrix;
 *
 *  QListIterator<QString> species_(this->species);
 *  QListIterator<QString> reactions_(this->reactions);
 *
 *  matrix << "[";
 *  for (int j = 0; species_.hasNext(); j++) {
 *    matrix << "[";
 *    reactions_.toFront();
 *    QString aSpecies = species_.next();
 *    for (int i = 1; reactions_.hasNext(); i++) {
 *      QString reaction = reactions_.next();
 *      matrix << QString("%1").arg( reactionsOrderMatrix.value(qMakePair(reaction,aSpecies), 0), 2 );
 *      if(reactions_.hasNext()) matrix << ", ";
 *    }
 *    matrix << "]";
 *    if(species_.hasNext()) matrix << ", ";
 *  }
 *  matrix << "]";
 *  return matrix.join("");
 *}
 */
