/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#include <QDebug>

#include "tabs/bifurcationtab.h"

#include <QDebug>
#include <QFileDialog>
#include <QJsonArray>
#include <QTextStream>

#include <fstream>
#include <iostream>
#include <utility>

#include "gnuplot_formats/svgformat.h"
#include "simulator.h"

using std::experimental::optional;
using std::experimental::nullopt;
using std::make_shared;
using std::make_unique;
using std::make_pair;
using std::pair;
using std::string;
using std::unique_ptr;
using std::shared_ptr;
using std::vector;

BifurcationTab::~BifurcationTab() = default;

BifurcationTab::BifurcationTab(unique_ptr<MainWindowState> mwstate, QWidget *parent, const optional<QJsonObject> &tabInfo_)
    : MainWindowTab(std::move(mwstate), parent),
      //ui_btab (shared_ptr<Ui::BifurcationTab>(new Ui::BifurcationTab(), [](Ui::BifurcationTab* ptr){ std::cout << "Cleaning Ui::BifurcationTab" << std::endl; delete ptr; })),
      gnuplot(make_unique<GnuPlot>(this) ),
      doneExecutingThread(true),
      saveDataAction(make_shared<QAction>(this)),
      replotAction  (make_shared<QAction>(this)) {

  qRegisterMetaType<ExecutionData>("ExecutionData");

  ui_btab.setupUi(this);
  ui_btab.rates_label->setAlignment(Qt::AlignCenter);
  //int qscroll_height = ui_btab.scrollable_column_widget->size().width();
  //ui_btab.scrollable_column_widget->resize(240, qscroll_height);
  ui_btab.number_of_experiments_label->setWordWrap(true);
  ui_btab.number_of_experiments_label->setText("Number of\nExperiments");
  ui_btab.number_of_experiments_label->setAlignment(Qt::AlignRight);
  ui_btab.total_final_steps_label->setWordWrap(true);
  ui_btab.total_final_steps_label->setText("Total Final\nSteps");
  ui_btab.total_final_steps_label->setAlignment(Qt::AlignRight);

  QStringList selected_rates;
  QStringList selected_species;
  // Detecting if there is extra info to fill the interface
  if( auto tabInfo = tabInfo_ ) {
    int num_exps    = static_cast<int>( tabInfo->value("number of experiments").toDouble() );
    int final_steps = static_cast<int>( tabInfo->value("total final steps").toDouble() );
    ui_btab.number_of_experiments_box->setText( QString("%1").arg( num_exps ) );
    ui_btab.total_final_steps_box    ->setText( QString("%1").arg( final_steps ) );

    selected_rates = tabInfo->value("rates").toObject().keys();
    for (const QJsonValue & a_speciesValue : tabInfo->value("species to plot").toArray()) {
      selected_species << a_speciesValue.toString();
    }
    //qDebug() << selected_species;
    bool axis_check = tabInfo->value("use first species to plot").toBool();
    ui_btab.useFirstRateForXAxisBox->setChecked(axis_check);
  }

  saveDataAction->setText("Save Data");
  saveDataAction->setEnabled(false); // disabling buttons to save data and plot
  replotAction->setText("Replot");
  replotAction->setEnabled(false); // disabling buttons to save data and plot

  actions.emplace_back(saveDataAction);
  actions.emplace_back(replotAction);

  connect(saveDataAction.get(), SIGNAL(triggered(bool)), this, SLOT(when_save_data_is_pushed(bool)));
  connect(replotAction.get(),   SIGNAL(triggered(bool)), this, SLOT(when_replot_is_pressed(bool)));

  // Adding reactions to chose from to reaction_selected_box
  QStringList reactions = this->mwstate->getReactions();
  for (int i = 0; i < reactions.size(); i++) {
    QString reaction = reactions.at(i);
    if ( tabInfo_ && selected_rates.contains(reaction) ) {
      selectedRates.emplace_back(reaction.toStdString());
      QJsonObject reactionObject = tabInfo_->value("rates").toObject().value(reaction).toObject();
      double startingPoint = reactionObject.value("from").toDouble();
      double endingPoint   = reactionObject.value("to").toDouble();
      addRateBoxesAndLabels(reaction, startingPoint, endingPoint);
    } else {
      ui_btab.reaction_selected_box->addItem(reaction);
    }
  }

  // Setting restrictions to what the user can enter into each box
  qintvalidator.setBottom(1);
  qdoublevalidator.setRange(0e0, std::numeric_limits<double>::max(), 16);

  QStringList species = this->mwstate->getSpecies();
  for (int i = 0; i < species.size(); i++) {
    QString a_species = species.at(i);
    auto speciesBox = make_shared<QCheckBox>(a_species);
    speciesToPlotCheckBoxes.push_back(speciesBox);
    ui_btab.selectSpeciesLayout->addWidget( speciesBox.get() );

    // setting to plot first two species
    if ( tabInfo_ && selected_species.contains(a_species) ) {
      speciesBox->setChecked(true);
    } else if(!tabInfo_ && i<2) {
      speciesBox->setChecked(true);
    }
  }

  /*
   *ui_btab.rate_step_size_box->setValidator(&qdoublevalidator);
   *ui_btab.initial_rate_box->setValidator(&qdoublevalidator);
   */
  ui_btab.number_of_experiments_box->setValidator(&qintvalidator);
  ui_btab.total_final_steps_box->setValidator(&qintvalidator);

  // when the image is generated (gnuplot finishes), the image shold be displayed
  connect(gnuplot.get(), SIGNAL(finished(int, QProcess::ExitStatus)), this,
          SLOT(when_image_is_generated(int, QProcess::ExitStatus)));

  connect(ui_btab.addRateButton, SIGNAL(clicked(bool)), this,
          SLOT(when_add_rate_is_clicked(bool)));
}

//BifurcationTab::~BifurcationTab() {
  //std::cout << "Cleaning BifurcationTab" << std::endl;
  //executingThread->join();
//}

void
BifurcationTab::execute(const ReactionDetails &rd,
                        const SimulationDetails<double> &simuDetails) {

  if(!doneExecutingThread) {
    std::cerr << "Trying to run but thread is still working" << std::endl;
    return;
  }

  if(selectedRates.empty()) {
    std::cerr << "BifurcationTab: Nothing to do, there aren't any selected rates" << std::endl;
    return;
  } // don't do anything if there are no rates to modify
  // TODO: add an alert telling the user to add a rate

  // Getting variables from the interface
  auto numberOfExperiments = static_cast<unsigned int>(ui_btab.number_of_experiments_box->text().toInt()); // e.g., => 100
  int totalFinalSteps = ui_btab.total_final_steps_box->text().toInt();     // e.g., => 80
  vector<string> selectedRates_(selectedRates);

  auto initialAndIntervalRates = getChangesForRates();

  vector<double> startingPointRates = std::get<0>( initialAndIntervalRates );
  vector<double> intervalRates      = std::get<1>( initialAndIntervalRates );

  // Getting which species are to be plotted
  QStringList speciesToPlot = getSpeciesToPlot();
  // checking if the x axis should show the values of the first reaction
  bool useFirstRateForXAxis = getUseFirstRateForXAxis(startingPointRates, intervalRates);

  doneExecutingThread = false;

  executingThread = make_unique<std::thread>(
   [this, totalFinalSteps, selectedRates_,
   startingPointRates, intervalRates, speciesToPlot, rd, simuDetails,
   useFirstRateForXAxis, numberOfExperiments]() {

    ReactionDetails rd_(rd);

    // variable to save results of all ran experiments
    vector<pair<vector<double>, SimulationResults>> runs;

    QStringList species   = rd_.getSpecies();

    for (std::size_t i = 0; i < numberOfExperiments; i++) {
      //std::cout << "." << std::flush;
      auto totalExps     = static_cast<double>(numberOfExperiments);
      auto progressValue = static_cast<int>(100 * i / totalExps);
      vector<double> runId = {static_cast<double>(i) };

      // setting something in the interface shouldn't be done in different thread, it may fail
      // the best thing to do is to call a method that is in the principal thread to repaint
      QMetaObject::invokeMethod(this,
                                "set_value_progress_bar",
                                Qt::AutoConnection,
                                Q_ARG(int, progressValue));

      // Setting new values for experiment
      for(unsigned j = 0; j < selectedRates_.size(); j+=1) {
        const string &rate = selectedRates_[j];
        double new_rate = startingPointRates[j] + intervalRates[j] * i;
        rd_.setRate(QString::fromStdString(rate), new_rate);
        //std::cout << rate << " " << new_rate << std::endl;
        runId.emplace_back( new_rate );
      }

      // running experiment and taking last 100 timeSteps from it
      auto run = Simulator::run(rd_, simuDetails, Simulator::Debug::Off).sliceEnd(totalFinalSteps);

      runs.emplace_back( std::move(runId), std::move(run) );
    }
    //std::cout << std::endl;

    vector<string> newCols = { "Run" };
    for (auto&& col : selectedRates_) {
      newCols.emplace_back( col );
    }
    SimulationResults runs_(newCols, runs);

    // Showing that computation has been finished
    QMetaObject::invokeMethod(this, "set_value_progress_bar", Qt::AutoConnection, Q_ARG(int, 100));

    // Creating svg image to display in GUI
    auto dataFile = make_shared<QTemporaryFile>();
    //dataFile->remove();
    if (dataFile->open()) {
      QTextStream stream(dataFile.get());
      //std::cout << runs_.toPlainText();
      stream << QString::fromStdString(runs_.toPlainText());
      dataFile->close();
    }

    QStringList selectedRatesQStringList;
    for (auto&& rate : selectedRates_) {
      selectedRatesQStringList.append(QString::fromStdString(rate));
    }

    execution_data = make_unique<ExecutionData>();
    execution_data->dataFile             = dataFile;
    execution_data->reactions            = selectedRatesQStringList;
    execution_data->Tmax                 = simuDetails.getTmax();
    execution_data->startX               = startingPointRates[0];
    execution_data->endX                 = startingPointRates[0] + intervalRates[0]*(numberOfExperiments-1);
    execution_data->numberOfExperiments  = numberOfExperiments;

    // Calling exec_gnuplot using Qt invoking method because this code is running in another thread and
    // it is not possible to execute gnuplot (QProcess) when it was created in another thread
    QMetaObject::invokeMethod(
        this,
        "exec_gnuplot",
        Qt::AutoConnection,
        Q_ARG(ExecutionData, *execution_data),
        Q_ARG(QStringList,   speciesToPlot),
        Q_ARG(bool,          useFirstRateForXAxis)
    );

    doneExecutingThread = true;

    saveDataAction->setEnabled(true); // careful, this may generate a horrible failure, TODO: consider other options
    replotAction->setEnabled(true);
   }
  );

  executingThread->detach();
}

bool
BifurcationTab::isStillRunning() {
  return !doneExecutingThread
         || gnuplot->state() != QProcess::ProcessState::NotRunning;  // It is not only gnuplot which is running
}

void
BifurcationTab::cleanTab() {
  ui_btab.qSvgWidget->load(QByteArray());
}

void
BifurcationTab::reapplyBoxesState() {
  for(const string &rate : selectedRates) {
    mwstate->setReactionsEnabled(rate, false);
  }
}

QStringList
BifurcationTab::getSpeciesToPlot() {
  QStringList speciesToPlot;
  for (auto&& aSpeciesBox : speciesToPlotCheckBoxes) {
    if(aSpeciesBox->isChecked()) {
      speciesToPlot << aSpeciesBox->text();
    }
  }
  return speciesToPlot;
}

pair<vector<double>, vector<double>>
BifurcationTab::getChangesForRates() {
  vector<double> startingPointRates
               , intervalRates;

  for(const string &rate : selectedRates) {
    // supposing that the user hasn't changed any value of selectedRatesBoxes
    // TODO(helq): Capture signal when one of the boxes from selectedRatesBoxes is changed so it deactivates replot
    startingPointRates.emplace_back( selectedRatesBoxes[rate].first->text().toDouble()  );
    double endingRate = selectedRatesBoxes[rate].second->text().toDouble();
    int numberOfExperiments;

    numberOfExperiments = ui_btab.number_of_experiments_box->text().toInt();
    intervalRates.emplace_back( (endingRate - startingPointRates.back()) / (numberOfExperiments-1) );
    //std::cout << rate << " " << (endingRate - startingPointRates.back()) / (numberOfExperiments-1) << std::endl;
  }
  return make_pair(startingPointRates, intervalRates);
}

bool
BifurcationTab::getUseFirstRateForXAxis(vector<double> startingPointRates, vector<double> intervalRates) {
  bool useFirstRateForXAxis = true;
  for(std::size_t i=1; i<startingPointRates.size(); i++) {
    if(  startingPointRates[0] != startingPointRates[i]
      ||      intervalRates[0] !=      intervalRates[i] )
    { useFirstRateForXAxis = false; }
  }
  // if the user selected the option "Use first rate for x axis" then that option it's forced
  return useFirstRateForXAxis || (ui_btab.useFirstRateForXAxisBox->checkState() == Qt::Checked);
}

void
BifurcationTab::when_image_is_generated(int exitCode, QProcess::ExitStatus exitStatus) { // NOLINT
  if (exitStatus == QProcess::ExitStatus::NormalExit) {
    ui_btab.qSvgWidget->load(gnuplot->getImageFilename());
    QByteArray gnuplotOutput = gnuplot->readAllStandardOutput();
    if (!gnuplotOutput.isEmpty()) { qDebug() << gnuplotOutput; }
  } else {
    qDebug() << "gnuplot didn't exit normally.";
    qDebug() << gnuplot->readAllStandardOutput();
  }
}

void
BifurcationTab::when_add_rate_is_clicked(bool checked) { // NOLINT
  if(ui_btab.reaction_selected_box->count() > 0) {

    // Preventing ugly stuff from happening, the user shouldn't try to replot given that the
    // executing info has changed
    replotAction->setEnabled(false);

    // getting index and text
    int     currentIndex = ui_btab.reaction_selected_box->currentIndex();
    QString indexTextQt  = ui_btab.reaction_selected_box->itemText(currentIndex);

    // adding to list of rates and deleting from combo box
    selectedRates.emplace_back(indexTextQt.toStdString());
    ui_btab.reaction_selected_box->removeItem(currentIndex);

    double rateForIndex = mwstate->getRate(indexTextQt);
    double startingPoint = rateForIndex/2;
    double endingPoint   = startingPoint*3;

    addRateBoxesAndLabels(indexTextQt, startingPoint, endingPoint);

    // updating state of rates shown on the principal column
    mwstate->enableAllBoxes();
    reapplyBoxesState();

    // if there are no reactions left to add, then disable button
    if(ui_btab.reaction_selected_box->count() == 0) {
      // there aren't any reactions/rates left to add
      ui_btab.addRateButtonWidget->setEnabled(false);
    }
  }
}

void
BifurcationTab::addRateBoxesAndLabels(QString indexTextQt, double startingPoint, double endingPoint) {
  string indexText = indexTextQt.toStdString();

  // Adding boxes with info about the added rate
  auto rateLabel        = make_shared<QLabel>(indexTextQt, this); // this is bad, but for some freaking reason Qt is in charge of cleaning all objects
  auto startingPointBox = make_shared<QLineEdit>(this);
  auto endingPointBox   = make_shared<QLineEdit>(this);
  selectedRatesLabels.emplace_back( rateLabel );
  selectedRatesBoxes.emplace( indexText, make_pair(startingPointBox, endingPointBox) );

  int lastPos = ui_btab.ratesLayout->rowCount();
  ui_btab.ratesLayout->addWidget(rateLabel.get(), lastPos, 0, 1, 2);
  ui_btab.ratesLayout->addWidget(startingPointBox.get(), lastPos+1, 0);
  ui_btab.ratesLayout->addWidget(endingPointBox.get(), lastPos+1, 1);

  QString startingPointStr = QString::number(startingPoint, 'g', 16);
  QString endingPointStr   = QString::number(endingPoint, 'g', 16);

  // setting values in boxes
  startingPointBox->setValidator(&qdoublevalidator);
  startingPointBox->setPlaceholderText(startingPointStr);
  startingPointBox->setText(startingPointStr);

  endingPointBox->setValidator(&qdoublevalidator);
  endingPointBox->setPlaceholderText(endingPointStr);
  endingPointBox->setText(endingPointStr);
}

void
BifurcationTab::exec_gnuplot(ExecutionData exec_data, QStringList speciesToPlot, bool useFirstRateForXAxis)
{
  if(speciesToPlot.size() == 0) {
    qDebug() << "No species has been selected to plot, select one and press replot";
    return;
  }

  auto format = std::make_shared<SvgFormat>();
  format->setWidth(ui_btab.qSvgWidget->size().width());
  format->setHeight(ui_btab.qSvgWidget->size().height());

  QStringList species = this->mwstate->getSpecies();

  format->setCustomPlotContent(
    [exec_data, speciesToPlot, useFirstRateForXAxis]
    (QString dataFilename, QStringList) -> QString {

      const QStringList &reactions = exec_data.reactions;
      double Tmax                  = exec_data.Tmax;
      double startX                = exec_data.startX;
      double endX                  = exec_data.endX;
      auto n_species = static_cast<unsigned int>(speciesToPlot.size());

      QStringList content;
      if (useFirstRateForXAxis) {
        content << QString(  "set xrange [%1:%2]").arg(QString::number(startX)).arg(QString::number(endX))
                << QString(R"(set xlabel 'Value of kinetic constant for "%1"')").arg(reactions[0])
                << QString(R"(set ylabel '"Final" concentration at time %1')").arg(QString::number(Tmax, 'g', 1))
                << QString(R"(plot '%1' using '%2':'%3',\)").arg(dataFilename, reactions[0], speciesToPlot[0]);

        for(std::size_t i = 1; i < n_species; i++) {
          content << QString("     '%1' using '%2':'%3'").arg(dataFilename, reactions[0], speciesToPlot[i])
                   + (i != n_species-1 ? QString(R"(,\)") : QString(""));
        }
      } else {
        content << QString(R"(set xlabel 'Number of Experiment')")
                << QString(R"(set ylabel '"Final" concentration at time %1')").arg(QString::number(Tmax, 'g', 1))
                << QString(R"(plot '%1' using 'Run':'%2',\)").arg(dataFilename, speciesToPlot[0]);

        for(std::size_t i = 1; i < n_species; i++) {
          content << QString("     '%1' using 'Run':'%2'").arg(dataFilename, speciesToPlot[i])
                   + (i != n_species-1 ? QString(R"(,\)") : QString(""));
        }
      }

      // printing gnuplot script
      // std::cout << std::endl << content.join("\n").toUtf8().data() << std::endl;

      return content.join("\n");
    }
  );

  gnuplot->execGnuPlot(exec_data.dataFile->fileName(), format, species, AskForExtraInfo::False);
}

void
BifurcationTab::set_value_progress_bar(int value) {
  ui_btab.progressBar->setValue(value);
}

void
BifurcationTab::when_save_data_is_pushed(bool checked) {
  qInfo() << "[Bifurcation tab]: Action triggered: Data Save";

  QString fileName;

  QFileDialog dialog(this);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setDirectory(QDir::homePath());
  dialog.setNameFilter(tr("Text File (*.txt)"));
  dialog.exec();

  if (dialog.selectedFiles().isEmpty()) return;

  fileName = dialog.selectedFiles().first();

  if (QFile::exists(fileName)) QFile::remove(fileName);
  if (!QFile::copy(execution_data->dataFile->fileName(), fileName))
    qInfo() << "Error when trying to save file" << fileName;
}

void
BifurcationTab::when_replot_is_pressed(bool checked) {
  // TODO: useFirstRateForXAxis shouldn't be altered by what values the user selects to change
  // when replotting
  auto initialAndIntervalRates = getChangesForRates();
  vector<double> startingPointRates = std::get<0>( initialAndIntervalRates );
  vector<double> intervalRates      = std::get<1>( initialAndIntervalRates );

  bool useFirstRateForXAxis = getUseFirstRateForXAxis(startingPointRates, intervalRates);
  QStringList speciesToPlot = getSpeciesToPlot();
  exec_gnuplot(*execution_data, speciesToPlot, useFirstRateForXAxis);
}

QJsonObject
BifurcationTab::toJsonObject() {
  QJsonObject ratesObject;
  for(const string &rate : selectedRates) {
    double startingPoint = selectedRatesBoxes[rate].first->text().toDouble();
    double endingPoint   = selectedRatesBoxes[rate].second->text().toDouble();
    QJsonObject rateValues {
      {"from", startingPoint},
      {"to",   endingPoint}
    };
    ratesObject.insert( QString::fromStdString(rate), rateValues );
  }

  QJsonArray speciesArray;
  for (const QString& aSpecies : getSpeciesToPlot()) {
    speciesArray << aSpecies;
  }

  return QJsonObject {
    {"type", "bifurcation"},
    {"rates", ratesObject},
    {"number of experiments", static_cast<double>(ui_btab.number_of_experiments_box->text().toInt()) },
    {"total final steps",     static_cast<double>(ui_btab.total_final_steps_box->text().toInt()) },
    {"use first species to plot", ui_btab.useFirstRateForXAxisBox->checkState() == Qt::Checked },
    {"species to plot", speciesArray}
  };
}
