/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_TABS_BIFURCATIONTAB_H_
#define SRC_TABS_BIFURCATIONTAB_H_

#include <QDoubleValidator>
#include <QIntValidator>
#include <QJsonObject>
#include <QTemporaryFile>

#include <memory>
#include <string>
#include <thread>
#include <vector>

#if defined(Q_OS_WIN)
#include "mingw.thread.h"
#endif

#include "optional.hpp"

#include "gnuplot.h"
#include "reactiondetails.h"
#include "simulationresults.h"
#include "tabs/mainwindowtab.h"
#include "ui_bifurcationtab.h"

/*!
 * @brief Tab that simulates how the change of a rate on an interval changes the final
 *        concentration values of a reaction
 *
 * This tab does the same job as StandardTab does, but it does it as many times as we like,
 * while variating some rate.
 */
class BifurcationTab : public MainWindowTab {
  Q_OBJECT

public:
  explicit BifurcationTab(
      std::unique_ptr<MainWindowState> mwstate,
      QWidget *parent = Q_NULLPTR,
      const std::experimental::optional<QJsonObject> & = std::experimental::nullopt);
  ~BifurcationTab();

  struct ExecutionData;

  void execute(const ReactionDetails &, const SimulationDetails<double> &);
  bool isStillRunning();
  void cleanTab();
  void reapplyBoxesState();
  QJsonObject toJsonObject();

  /*!
   * @brief Returns the currently selected species to plot
   *
   * @return species to plot
   */
  QStringList getSpeciesToPlot();
  /*!
   * @brief Calculates the change step for each selected rate
   *
   * @return For each selected rate we get the initial rate (kinetic constant) and
   *         the step size of change of that rate
   */
  std::pair<std::vector<double>, std::vector<double>> getChangesForRates();
  /*!
   * @brief Detects if the plot should name the x-axis using the change on the first rate
   *        or should shouw the experiment number
   *
   * @param startingPointRates List of initial rates (kinetic constant) to start the bifurcation
   * @param intervalRates      Size of the step to change in each experiment
   *
   * @return Whether to use or not the first rate change in the x-axis
   */
  bool getUseFirstRateForXAxis(std::vector<double>, std::vector<double>);

private slots:
  /*!
   * @brief if creating the bifurcation plot succeded then it will try to show it in the tab
   */
  void when_image_is_generated(int, QProcess::ExitStatus);
  /*!
   * @brief creates boxes to set the range in which the selected rate is meant to change
   *        in the simulations
   *
   * @param checked This parameter is ignored, it is necessary to connect this slot with the
   *                signal finished from the button
   *
   * When the button to add a rate is clicked, the current selected reaction in the
   * ComboBox is selected to be part of the rates. Two new boxes are created and
   * displayed on the ui.
   */
  void when_add_rate_is_clicked(bool);
  /*!
   * @brief calls gnuplot to create plot from the data generated in the simulation
   *
   * @param exec_data            Data generated in the simulation
   * @param speciesToPlot        A list with all the species to show in the plot
   * @param useFirstRateForXAxis Whether to use the first selected reaction to label the x-axis of the plot
   */
  void exec_gnuplot(ExecutionData, QStringList, bool);
  /*!
   * @brief Changes the vale of the progress bar ubicated at the bottom of the tab
   *
   * @param int  Percentage between 0-100 that indicates the percentage of iterations
   *             done on the simulation
   *
   * @param checked This parameter is ignored, it is necessary to connect this slot with the
   *                signal finished from the button
   *
   * The value of the progress bar cannot be changed from another thread, when one tries
   * to do so the program crashes, but a slot CAN be "invoked" from another thread, thus
   * this slot is just a wrapping to a method that is not a slot.
   */
  void set_value_progress_bar(int);
  /*!
   * @brief Shows a dialog to the user to save the plot when "Tab -> Save Plot"
   *        option is pressed
   *
   * @param checked This parameter is ignored, it is necessary to connect this slot with the
   *                signal finished from the button
   */
  void when_save_data_is_pushed(bool);
  /*!
   * @brief Plots the simulation results. It has into account the current selected species
   *
   * @param checked This parameter is ignored, it is necessary to connect this slot with the
   *                signal finished from the button
   */
  void when_replot_is_pressed(bool);

private:
  /*!
   * @brief General structure of the ui, Ui::BifurcationTab is automatically generated
   *        from `src/tabs/ui/bifurcationtab.ui`
   */
  Ui::BifurcationTab ui_btab;
  /*!
   * @brief Current simulation data, this data is newly generated each time `execute` is
   *        called
   *
   * Because this data is saved in a `unique_ptr`, everytime the data is overwritten by
   * the result of a new executing new execution
   */
  std::unique_ptr<ExecutionData> execution_data;

  /*!
   * @brief A pointer to a QProcess object in charge of executing gnuplot
   */
  std::unique_ptr<GnuPlot> gnuplot;

  QIntValidator    qintvalidator;    //!< Integer validator used to restrict what is entered in some boxes
  QDoubleValidator qdoublevalidator; //!< Double validator used to restrict what is entered in some boxes

  /*!
   * @brief Contains the selected reactions whose kinetic constant (rate) will be changed
   *        for the bifurcation simulation
   */
  std::vector<std::string> selectedRates;
  /*!
   * @brief Contains QCheckBox's of each one of the species to plot
   */
  std::vector<std::shared_ptr<QCheckBox>> speciesToPlotCheckBoxes;
  /*!
   * @brief stores the boxes on which the user will input the minimum and maximum values
   *        for a rate in the simulations
   */
  std::unordered_map<
    std::string,
    std::pair< std::shared_ptr<QLineEdit>,
               std::shared_ptr<QLineEdit> >
    > selectedRatesBoxes;

  /*!
   * @brief dummy vector (it is never called by any method)
   *
   * This vector just holds a reference to every instance of QLabel, so that when the
   * destructor of BifurcationTab is called all QLabel's correctly freed from memory
   */
  std::vector<std::shared_ptr<QLabel>> selectedRatesLabels;

  /*!
   * @brief Thread where the simulations are going to be performed.
   *
   * This thread is used to perform the simulations without freezing the user interface.
   * To prevent erroneous behaivor, e.g., deleting this thread with the first finishing,
   * this thread uses the atomic boolean doneExecutingThread.
   */
  std::unique_ptr<std::thread> executingThread;
  /*!
   * @brief Saves the current state of the thread, if it the thread is still running then
   *        this boolean is false
   *
   * To prevent racing conditions, e.g., trying to run the simulations without finishing
   * the current ones, this boolean keeps the current state of the thread.
   */
  std::atomic<bool> doneExecutingThread;

  /*!
   * @brief Action to be shown in the menu "Tab". When pressed it calls when_save_data_is_pushed
   */
  std::shared_ptr<QAction> saveDataAction;
  /*!
   * @brief Action to be performed when the user wants to, it replots taking into account the
   *        current selected species
   */
  std::shared_ptr<QAction> replotAction;

  /*!
   * @brief Creates labels and boxes where the user will input the ranges to vary the rate
   *
   * @param QString Name of the rate to change
   * @param double  Starting point
   * @param double  Ending point
   */
  void addRateBoxesAndLabels(QString, double, double);
};

/*!
 * @brief Data generated from a bifurcation simulation
 *
 * Each element of the struct is of a type that Qt understands, this struct is by the dafault
 * not one of such types, and it's therefore necessary to declare explicitly that this
 * struct is a type Qt understands using the directive Q_DECLARE_METATYPE
 *
 * Why is it necessary that Qt understands this type? Because this type/struct is used in a slot
 * and slots only understand types/structs Qt understands.
 */
struct BifurcationTab::ExecutionData {
  std::shared_ptr<QTemporaryFile> dataFile; //!< temporary file where the simulated data is saved
  QStringList reactions; //!< Reactions which are changed in the simulations
  double      Tmax;      //!< Tmax used when running the simulation
  double      startX;    //!< First value used for the first rate in the simulation, aka starting point
  double      endX;      //!< Last value used for the first rate in the simulation, aka starting point
  int         numberOfExperiments; //!< Number of experiments performed in the last run
};

Q_DECLARE_METATYPE(BifurcationTab::ExecutionData);

#endif  // SRC_TABS_BIFURCATIONTAB_H_
