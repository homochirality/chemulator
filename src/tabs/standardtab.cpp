/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#include <QDebug>

#include "tabs/standardtab.h"

#include <QDebug>
#include <QFileDialog>
#include <QGridLayout>
#include <QTextStream>

#include <iostream>

#include "gnuplot_formats/epsformat.h"
#include "gnuplot_formats/gnuplotformat.h"
#include "gnuplot_formats/pbmformat.h"
#include "gnuplot_formats/pngformat.h"
#include "gnuplot_formats/svgformat.h"
#include "qt_version_compatibility.h"
#include "simulationresults.h"
#include "simulator.h"

using std::shared_ptr;
using std::unique_ptr;

StandardTab::StandardTab(unique_ptr<MainWindowState> mwstate, QWidget *parent)
    : MainWindowTab(std::move(mwstate), parent),
      gnuplot       (std::make_unique<GnuPlot>       (this)),
      dataFile      (std::make_unique<QTemporaryFile>()    ),
      svgWidget     (std::make_unique<QSvgWidget>    (this)),
      saveDataAction(std::make_shared<QAction>       (this)),
      savePlotAction(std::make_shared<QAction>       (this)),
      layout        (std::make_unique<QGridLayout>   (this))  // Creating layout for this Tab
{

  this->setMinimumSize(630, 430);

  layout->addWidget(svgWidget.get(), 0, 0);

  saveDataAction->setText("Save Data");
  savePlotAction->setText("Save Plot");
  // disabling buttons to save data and plot
  saveDataAction->setEnabled(false);
  savePlotAction->setEnabled(false);

  actions.emplace_back(saveDataAction);
  actions.emplace_back(savePlotAction);

  connect(saveDataAction.get(), SIGNAL(triggered(bool)), this, SLOT(when_save_data_is_pushed(bool)));
  connect(savePlotAction.get(), SIGNAL(triggered(bool)), this, SLOT(when_save_plot_is_pushed(bool)));

  // when the image is generated (gnuplot finishes), the image shold be displayed
  connect(gnuplot.get(), SIGNAL(finished(int, QProcess::ExitStatus)), this,
          SLOT(when_image_is_generated(int, QProcess::ExitStatus)));
}

//StandardTab::~StandardTab() {
//  std::cout << "Cleaning StandardTab" << std::endl;
//}

void
StandardTab::execute(const ReactionDetails &rd,
                     const SimulationDetails<double> &simulationDetails) {

  SimulationResults run = Simulator::run(rd, simulationDetails);

  //std::cout << "Inside execute. Time intervals " << run.getNumTimeIntervals() << std::endl;

  // Creating svg image to display in GUI
  dataFile->remove();
  if (dataFile->open()) {
    QTextStream stream(dataFile.get());
    //std::cout << run.toPlainText();
    stream << QString::fromStdString(run.toPlainText());
    dataFile->close();
  }
  //std::cout << "File created" << std::endl;

  auto format = std::make_shared<SvgFormat>();

  format->setWidth(svgWidget->size().width());
  format->setHeight(svgWidget->size().height());

  gnuplot->execGnuPlot(dataFile->fileName(), format, rd.getSpecies(), AskForExtraInfo::False);

  saveDataAction->setEnabled(true);
  savePlotAction->setEnabled(true);
}

bool
StandardTab::isStillRunning() {
  return gnuplot->state() !=
         QProcess::ProcessState::NotRunning;  // It is not only gnuplot which is running, but gnuplot
}

void
StandardTab::when_image_is_generated(int exitCode, QProcess::ExitStatus exitStatus) {
  if (exitStatus == QProcess::ExitStatus::NormalExit) {
    svgWidget->load(gnuplot->getImageFilename());
    QByteArray gnuplotOutput = gnuplot->readAllStandardOutput();
    if (!gnuplotOutput.isEmpty()) qDebug() << gnuplotOutput;
  } else {
    qDebug() << "gnuplot didn't exit normally.";
    qDebug() << gnuplot->readAllStandardOutput();
  }
}

void
StandardTab::cleanTab() {
  svgWidget->load(QByteArray());
}

void
StandardTab::when_save_data_is_pushed(bool checked) {
  qInfo() << "Action triggered: Data Save";

  QString fileName;

  QFileDialog dialog(this);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setDirectory(QDir::homePath());
  dialog.setNameFilter(tr("Text File (*.txt)"));
  dialog.exec();

  if (dialog.selectedFiles().isEmpty()) return;

  fileName = dialog.selectedFiles().first();

  if (QFile::exists(fileName)) QFile::remove(fileName);
  if (!QFile::copy(dataFile->fileName(), fileName)) qInfo() << "Error when trying to save file" << fileName;
}

void
StandardTab::when_save_plot_is_pushed(bool checked) {
  qInfo() << "Action triggered: Plot Save";

  QString eps("PostScript file(*.eps)");
  QString pbm("PBM image(*.pbm)");
  QString svg("SVG image(*.svg)");
  QString png("PNG image(*.png)");

  // getting file name
  QString fileName;

  QFileDialog dialog(this);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setDirectory(QDir::homePath());
  dialog.setNameFilter(eps + ";;" + pbm + ";;" + svg + ";;" + png);
  dialog.exec();

  if (dialog.selectedFiles().isEmpty()) return;

  fileName = dialog.selectedFiles().first();

  qDebug() << "Filter selected" << dialog.selectedNameFilter();

  shared_ptr<GnuPlotFormat> format;
  QString selFormat = dialog.selectedNameFilter();

  if (selFormat == eps)
    format = std::make_shared<EpsFormat>();
  else if (selFormat == pbm)
    format = std::make_shared<PbmFormat>();
  else if (selFormat == png)
    format = std::make_shared<PngFormat>();
  else
    format = std::make_shared<SvgFormat>();

  format->setWidth(800);
  format->setHeight(600);

  // running gnuplot (it'll save the file)
  GnuPlot gnuplot_image_to_save(this);
  gnuplot_image_to_save.setImageFilename(fileName);

  QString dataFileName = dataFile->fileName();

  gnuplot_image_to_save.execGnuPlot(dataFileName, format, mwstate->getSpecies(), AskForExtraInfo::True);
  // qDebug() << "Executing gnuplot";
  gnuplot_image_to_save.waitForFinished();
  // QThread::sleep(30);
}

void
StandardTab::reapplyBoxesState() {}

QJsonObject
StandardTab::toJsonObject() {
  return QJsonObject {
    {"type", "standard"}
  };
}
