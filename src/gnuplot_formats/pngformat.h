/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_GNUPLOT_FORMATS_PNGFORMAT_H_
#define SRC_GNUPLOT_FORMATS_PNGFORMAT_H_

#include <QObject>

#include "gnuplot_formats/gnuplotformat.h"

/*!
 * @brief Creates a gnuplot script with the image format PNG
 */
class PngFormat : public GnuPlotFormat {
  Q_OBJECT

public:
  PngFormat() = default;
  ~PngFormat() = default;

  const QString scriptHeader(QString imageFilename);
  CancelButton askDetailsOfFormat(QWidget *parent);

  // private:
};

#endif  // SRC_GNUPLOT_FORMATS_PNGFORMAT_H_
