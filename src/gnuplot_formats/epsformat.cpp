/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gnuplot_formats/epsformat.h"
#include <QDebug>
#include <QFile>
#include <QSpinBox>

using std::tuple;
using std::shared_ptr;
using std::unique_ptr;

EpsFormat::EpsFormat() { take_every = 1; }

const QString
EpsFormat::scriptHeader(QString imageFilename) {
  return QString("set terminal postscript eps\nset output '%1'").arg(imageFilename);
}

CancelButton
EpsFormat::askDetailsOfFormat(QWidget *parent) {
  qDebug() << "Asking details for eps";

  tuple<unique_ptr<QDialog>, unique_ptr<QFormLayout>, shared_ptr<CancelButton>>
    dialogAndForms = createDialogWindowAndFormLayout(parent);

  auto msgWindow    = std::move( std::get<0>(dialogAndForms) );
  auto forms        = std::move( std::get<1>(dialogAndForms) );
  auto cancelButton = std::get<2>(dialogAndForms);

  QSpinBox takeEveryBox;
  takeEveryBox.setSpecialValueText("10");
  takeEveryBox.setRange(1, 10000000);

  forms->addRow("Take a sample point every (n) points", &takeEveryBox);

  // showing dialog window
  msgWindow->exec();

  take_every = takeEveryBox.text().toInt();

  return *cancelButton;
}
