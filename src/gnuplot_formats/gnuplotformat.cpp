/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gnuplot_formats/gnuplotformat.h"
#include <iostream>

//#include <QDebug>
#include <QFormLayout>
#include <QPair>

#include "ui/msgwindowgnuplotformat.h"

using std::shared_ptr;
using std::unique_ptr;
using std::tuple;

const QString
GnuPlotFormat::script(QString imageFilename, QString dataFilename, QStringList speciesToPlot) {
  QString header = scriptHeader(imageFilename);
  QString content;

  if (customPlotContent != nullptr)
    content = customPlotContent(dataFilename, speciesToPlot);
  else
    content = speciesVSTimeContent(dataFilename, speciesToPlot);

  // qDebug() << header;
  // qDebug() << content;

  return header + "\n" + content;
}

tuple<unique_ptr<QDialog>, unique_ptr<QFormLayout>, shared_ptr<CancelButton>>
GnuPlotFormat::createDialogWindowAndFormLayout(QWidget *parent) {
  auto msgWindow = std::make_unique<Ui::MsgWindowGnuPlotFormat>(parent);

  // form to ask specific questions
  auto forms = std::make_unique<QFormLayout>(msgWindow->enclosingFormLayout.get());

  QObject::connect(msgWindow->buttonBox.get(), SIGNAL(accepted()), msgWindow.get(), SLOT(accept()));
  QObject::connect(msgWindow->buttonBox.get(), SIGNAL(rejected()), msgWindow.get(), SLOT(reject()));

  auto cancelButton = std::make_shared<CancelButton>();
  *cancelButton = CancelButton::NotPressed;
  connect(msgWindow.get(), &QDialog::rejected,
      [cancelButton]() mutable {
        *cancelButton = CancelButton::Pressed;
      });

  return std::make_tuple(std::move(msgWindow), std::move(forms), cancelButton);
}

const QString
GnuPlotFormat::speciesVSTimeContent(QString dataFilename, QStringList species) {
  int number_of_species = species.length();
  bool two_together = false;
  QStringList gnuplotScript;

  switch (number_of_species) {
    case 2:
      gnuplotScript << "set multiplot layout 2, 1";
      break;
    case 3:
      gnuplotScript << "set multiplot layout 3, 1";
      break;
    case 4:
      gnuplotScript << "set multiplot layout 2, 2";
      break;
    case 5:
    case 6:
      gnuplotScript << "set multiplot layout 3, 2";
      break;
    case 7:
    case 8:
      gnuplotScript << "set multiplot layout 2, 2 rowsfirst";
      two_together = true;
      break;
    default: break;
  }

  QString every = QString::number(this->take_every);

  for(int i=0; i<number_of_species; i++) {
    if (two_together && i<number_of_species-1) {
      //std::cout << species.at(i).toStdString() << std::endl;
      QString aSpecies1 = species.at(i);
      QString aSpecies2 = species.at(++i);

      // %3 will be replaced for the name of the data file
      // %4 will be replaced for the distance between points that will be taken to create the figure
      gnuplotScript << QString("set title '%1 vs %2'").arg(aSpecies1, aSpecies2)
                    << QString("plot '%3' using 'Time':'%1' every %4 title '%1' with points linecolor 1 pointsize 0.9,"
                                   " '%3' using 'Time':'%2' every %4 title '%2' with lines  linecolor 5"
                       ).arg(aSpecies1, aSpecies2, dataFilename, every);
    } else if (number_of_species<=8) {
      QString aSpecies = species.at(i);

      gnuplotScript << QString("set title '%1'").arg(aSpecies)
                    << QString("plot '%2' using 'Time':'%1' every %3 title '%1' with points linecolor 1 pointsize 0.9"
                       ).arg(aSpecies, dataFilename, every);
    } else {
      QString aSpecies = species.at(i);

      QString plot = QString(i==0 ? "plot " : "   , " );

      plot += QString("'%2' using 'Time':'%1' every %3 title '%1' with points linecolor 1 pointsize 0.9"
                    ).arg(aSpecies, dataFilename, every);
      if (i<number_of_species-1) {
        plot += " \\";
      }

      gnuplotScript << plot;

    }
  }

  if ( number_of_species <= 2 )
     gnuplotScript << "unset multiplot";

  return gnuplotScript.join( "\n" );
}
