/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gnuplot.h"
#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <iostream>

// variable defined by CMake
#define gnuplotExec GNUPLOT_EXECUTABLE

using std::shared_ptr;

GnuPlot::GnuPlot(QWidget *_parent)
  : QProcess(_parent)
{
  this->parent = _parent;
  if (tmpdir.isValid()) {
    qDebug() << "Temporal path for GnuPlot data:" << tmpdir.path();
    QString tmppath = tmpdir.path();

    imageFilename = tmppath + "/image.svg";
    gnuplotScriptFilename = tmppath + "/generate_image.gpi";
  } else {
    qDebug() << "Error: temporal folder couldn't be created, please check your permissions of in the system "
                "temporal folder";
  }
}

//GnuPlot::~GnuPlot() {
//  std::cout << "Cleaning GnuPlot";
//}

void
GnuPlot::execGnuPlot(const QString &dataFilename,
                     shared_ptr<GnuPlotFormat> format,
                     const QStringList &speciesToPlot,
                     AskForExtraInfo extraInfo) {

  if (extraInfo == AskForExtraInfo::True) {
    auto cancelButton = format->askDetailsOfFormat(parent);
    switch (cancelButton) {
      case CancelButton::Pressed:
        return;  // if cancel button has been pressed, exit
      default:;
    }
  }

  QFile file(gnuplotScriptFilename);
  // QIODevice::WriteOnly truncates an already written file
  if (file.open(QIODevice::WriteOnly)) {
    QTextStream out(&file);
    QString script = format->script(imageFilename, dataFilename, speciesToPlot);
    out << script;
    // qDebug("%s\n", script.toUtf8().data());
    file.close();
  }

  qDebug() << "Executing gnuplot with params: " << gnuplotExec << gnuplotScriptFilename;
  start(gnuplotExec, QStringList(gnuplotScriptFilename));
}
